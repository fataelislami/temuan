<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MBroadcast extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function getfollowers($id_official_location){
    $query="SELECT COUNT(`id_user`) as `followers` FROM `circle` where `id_official_location`=$id_official_location";
    return $this->db->query($query);
  }

  function getdata($where,$from){
    $this->db->where($where);
    $db=$this->db->get($from);
    return $db;
  }


}
