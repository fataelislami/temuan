<script>
    <?php
    if($this->session->flashdata('message')) { ?>
        alert('<?php echo $this->session->flashdata('message');?>'); <?php
    } ?>
</script>
<div class="row">
  <!-- Column -->
  <div class="col-md-12">
     <div class="card card-primary card-inverse">
        <div class="box text-center">
           <h1 class="font-light text-white"><?php echo $get->followers; ?></h1>
           <h6 class="text-white">Total Target Users</h6>
        </div>
     </div>
  </div>
</div>

<!-- vertical wizard -->
<div class="row">
   <div class="col-12">
      <div class="card">
         <div class="card-body wizard-content ">
            <h4 class="card-title">Sebarkan dengan cara apa nih gans!</h4>
            <h6 class="card-subtitle">jangan lupa bismillah</h6>
            <form action="<?php echo base_url()?>member/broadcast/process" method="post" class="tab-wizard vertical wizard-circle">
               <!-- Step 1 -->
               <input type="hidden" name="city" value="<?php echo $city ?>">
               <h6>Personal Info</h6>
               <section>
                 <div class="row">
                    <div class="col-md-12">
                       <div class="form-group">
                          <label for="location1">Type Broacast :</label>
                          <select id="selectType" onchange="changeFunc();" class="custom-select form-control" name="type">
                             <option value="">Select Type</option>
                             <option value="image">Image</option>
                             <option value="audio">Audio</option>
                             <option value="video">Video</option>
                          </select>
                       </div>
                    </div>
                 </div>
                  <div class="row">
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="alttext">Alt text :</label>
                           <input type="text" name="alt-text" class="form-control" required="required">
                        </div>
                        <div class="form-group">
                           <label for="url">Url or Base Url :</label>
                           <input type="url" name="url" class="form-control" required="required">
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="firsttext">Description Text :</label>
                           <textarea name="first-text" class="form-control" required="required"></textarea>
                        </div>
                        <div class="form-group">
                           <label for="size" id="labelSize" hidden disabled>Size Image Url :</label>
                           <input type="text" id="size" name="size" class="form-control" required="required" hidden disabled>
                        </div>
                     </div>
                  </div>

               </section>
               <button type="submit" name="submit" class="btn btn-info" name="button">SUBMIT</button>

               <!-- Step 4 -->
               <!-- <h6>Remark</h6>
               <section>
                  <div class="row">
                     <div class="col-md-6">

                        <div class="form-group">
                           <label for="participants1">Result</label>
                           <select class="custom-select form-control" id="participants1" name="location">
                              <option value="">Select Result</option>
                              <option value="Selected">Selected</option>
                              <option value="Rejected">Rejected</option>
                              <option value="Call Second-time">Call Second-time</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-6">
                        <div class="form-group">
                           <label for="decisions1">Comments</label>
                           <textarea name="decisions" id="decisions1" rows="4" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                           <label>Rate Interviwer :</label>
                           <div class="c-inputs-stacked">
                              <label class="inline custom-control custom-checkbox block">
                              <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">1 star</span> </label>
                              <label class="inline custom-control custom-checkbox block">
                              <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">2 star</span> </label>
                              <label class="inline custom-control custom-checkbox block">
                              <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">3 star</span> </label>
                              <label class="inline custom-control custom-checkbox block">
                              <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">4 star</span> </label>
                              <label class="inline custom-control custom-checkbox block">
                              <input type="checkbox" class="custom-control-input"> <span class="custom-control-indicator"></span> <span class="custom-control-description ml-0">5 star</span> </label>
                           </div>
                        </div>
                     </div>
                  </div>
               </section> -->
            </form>
         </div>
      </div>
   </div>
</div>
