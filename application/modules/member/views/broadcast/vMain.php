<script>
    <?php
    if($this->session->flashdata('message')) { ?>
        alert('<?php echo $this->session->flashdata('message');?>'); <?php
    } ?>
</script>
<!-- vertical wizard -->
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body wizard-content ">
                <h4 class="card-title">Multicast by Location</h4>
                <h6 class="card-subtitle">Temuan Target</h6>
                <form action="<?php echo base_url()?>admin/broadcast" class="tab-wizard vertical wizard-circle">
                    <!-- Step 1 -->
                    <h6>Personal Info</h6>
                    <section>

                        <div class="row">

                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="date1">Broadcast ke lokasi mana nih? :</label>
                                    <input type="text" name="city" class="form-control" id="text"> </div>
                            </div>
                        </div>
                    </section>


                </form>
            </div>
        </div>
    </div>
</div>
