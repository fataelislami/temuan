<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
 } ?>
<br><br>
<div class="card" style="">
    <div class="card-body">
        <h4 class="card-title">Laporan Anda</h4>
        <h6 class="card-subtitle">Temuan</h6>
        <table id="demo-foo-addrow2" class="table table-bordered table-hover toggle-circle" data-page-size="7">
            <thead>
                <tr>
                    <th data-sort-initial="true" data-toggle="true">Status</th>
                    <th>Barang</th>
                    <th data-hide="phone, tablet">Deskripsi</th>
                    <th data-hide="phone, tablet">Waktu</th>
                    <th data-hide="phone, tablet">Aksi</th>
                    <th data-hide="phone, tablet">Broadcast</th>
                    <th data-sort-ignore="true" class="min-width">Aksi</th>
                </tr>
            </thead>
            <div class="m-t-40">
                <div class="d-flex">

                        <div class="form-group">
                            <input id="demo-input-search2" type="text" placeholder="Search" autocomplete="off">
                        </div>
                </div>
            </div>
            <?php if($datathings!=null){ ?>
            <tbody>
              <?php foreach ($datathings as $d): ?>
                <tr>
                    <td><span class="label label-table label-success"><?php echo $d->status; ?></span></td>
                    <td><?php echo $d->name; ?></td>
                    <td><?php echo $d->desc ?></td>
                    <td><?php echo $d->datetime ?></td>
                    <td>
                      <a href="<?php echo base_url().$module?>/things/edit/<?php echo $d->id_things ?>">
                      <button class="btn btn-success waves-effect waves-light m-r-10">Edit</button>
                    </a>
                    <button  data-toggle="modal" data-target="#responsive-modal" class="btn btn-danger waves-effect waves-light m-r-10 modalDelete" value="<?php echo $d->id_things ?>">Delete</button>
                    </td>
                    <td>
                      <button  data-toggle="modal" data-target="#responsive-modal" class="btn btn-danger waves-effect waves-light m-r-10 modalBroadcastLocation" value="<?php echo $d->id_things."#".$d->status ?>">Broadcast</button>
                    </td>
                    <td>
                      <!-- modal -->
                      <button type="button" class="btn btn-primary waves-effect waves-light m-r-10" data-toggle="modal" data-target="#viewModal<?php echo $d->id_things ?>" data-whatever="@mdo" >View</button>
                      <div class="modal fade bs-example-modal-lg" id="viewModal<?php echo $d->id_things ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                              <div class="modal-dialog modal-lg" role="document">
                                                  <div class="modal-content">
                                                      <div class="modal-header">
                                                          <h4 class="modal-title" id="myLargeModalLabel">Detail Barang</h4>
                                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                      </div>
                                                      <div class="modal-body">

                                                        <!-- Modal Body -->
                                                        <div class="row">
                                                          <div class="col-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                  <table width="100%">
                                                                    <tr>
                                                                      <td>Nomor</td><td><?php echo $d->id_things?></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td>Name</td><td><?php echo $d->name?></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td>Deskripsi</td><td><?= substr($d->desc,0,20)?></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <td>Nama User</td><td><?php echo $d->nameuser?></td>
                                                                    </tr>
                                                                    <tr>
                                                                      <center>
                                                                      <img src="https://islamify.id/bot/temuan/xfile/laporan/<?php echo "$d->imagename"; ?>" style="width:80%;height:auto;" alt="">
                                                                    </tr>
                                                                  </table>
                                                                </div>
                                                            </div>
                                                          </div>
                                                        </div>
                                                        <!-- /.model body -->

                                                      </div>
                                                      <div class="modal-footer">
                                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                          <!-- /.modal -->
                                          <?php if($d->verification=='pending'){ ?>
                                            <button  data-toggle="modal" data-target="#responsive-modal" class="btn btn-info waves-effect waves-light m-r-10 modalVerification" value="<?php echo $d->id_things ?>">Verifikasi</button>
                                          <?php }else{ ?>
                                            <span class="label label-table label-success"><?php echo $d->verification ?></span>
                                          <?php } ?>
                    </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          <?php } ?>
            <tfoot>
                <tr>
                    <td colspan="6">
                        <div class="text-right">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<!-- delete modal content -->
<div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Temuan Hi!</h4>
            </div>
            <div class="modal-body">
              <p id="modalMsg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                <a id="modalHref" href="#">
                <button type="button" class="btn btn-danger waves-effect waves-light">Yup!</button>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
