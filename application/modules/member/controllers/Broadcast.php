<?php
require_once('class/MessageBuilder.php');
defined('BASEPATH') OR exit('No direct script access allowed');

class Broadcast extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('MBroadcast','Dbs'));
    if($this->session->userdata('status')!='login'){
      redirect(base_url('login'));
    }
    if($this->session->userdata('role')!=2){
      redirect(redirect($_SERVER['HTTP_REFERER']));
    }
  }

  function index()
  {
      $get=$this->MBroadcast->getfollowers($this->session->userdata('id'))->row();
      $data = array(
        'contain_view' => 'member/broadcast/vMainParam',
        'sidebar'=>'member/sidebar',//Ini buat menu yang ditampilkan di module member {DIKIRIM KE TEMPLATE}
        'css'=>'member/broadcast/assets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'member/broadcast/assets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'nama'=>'Kang Ify!',
        'get'=>$get,
        'city'=>"2000"//ngirim variable ke view yang ada di module member {DIKIRIM KE VIEW member}
       );

    // $this->load->view('home_v', $data);
    $this->template->load($data);

  }

  function process(){
    $send= new MessageBuilder();
    if(isset($_POST['submit'])){
      include 'Multicast.php';
      $multicast=new Multicast();
      $url=$_POST['url'];
      $type=$_POST['type'];
      $alt=$_POST['alt-text'];
      $firsttext=$_POST['first-text'];
      $city=$_POST['city'];
      $db=$this->Dbs->getdata(array('id_official_location'=>$this->session->userdata('id')),'circle');
      if($type=='image'){
        $ballons=[];
        $ballon1=$send->text($firsttext);
        $ballon2=$send->image($url);
        array_push($ballons,$ballon1,$ballon2);
    		$multicast->send($db,$ballons);
        echo "status";
        $this->session->set_flashdata('message', 'Terkirim Cuy!');
        redirect(site_url('member/broadcast'));
      }else if($type=='audio'){
        $ballons=[];
        $ballon1=$send->text($firsttext);
        $ballon2=$send->audio($url);
        array_push($ballons,$ballon1,$ballon2);
    		$multicast->send($db,$ballons);
        echo "status";
        $this->session->set_flashdata('message', 'Terkirim Cuy!');
        redirect(site_url('member/broadcast'));
      }else if($type=='video'){
        $ballons=[];
        $ballon1=$send->text($firsttext);
        $ballon2=$send->video($url,"https://islamify.id/bot/temuan/xfile/images/belum_ada_foto.png");
        array_push($ballons,$ballon1,$ballon2);
    		$multicast->send($db,$ballons);
        echo "status";
        $this->session->set_flashdata('message', 'Terkirim Cuy!');
        redirect(site_url('member/broadcast'));
      }else{
        $this->session->set_flashdata('message', 'Gagal Cuy!');
        redirect(site_url('member/broadcast'));
      }
    }
  }

}
