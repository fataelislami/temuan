<?php
require_once('class/line_class.php');
require_once('class/MessageBuilder.php');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Things extends MY_Controller
{
  private $channelAccessToken = 'Zp9Sz4p8N5rnosRwDt+CeGDHWa9yD9QAgZGMmFMo/Ehnty8ZUv46FZMlqs42C3C4jsMGzKmcI3SriBE98Dd6aLswgHoH8aNJWUsmC8EGozvfnwDaNNzntIZQy3UMpX1jomdlWSI0jItk6sDLbj2TqgdB04t89/1O/w1cDnyilFU=';
  private $channelSecret = '2c0762c35a8b2227bc591505aa4cc833';
    function __construct()
    {
        parent::__construct();
        $this->load->model('Things_model');
        $this->load->model('Dbs');
        $this->load->library('form_validation');
        if($this->session->userdata('status')!='login'){
          redirect(base_url('login'));
        }
        if($this->session->userdata('role')!=2){
          redirect(redirect($_SERVER['HTTP_REFERER']));
        }
    }

    public function index()
    {

      $datathings=$this->Things_model->get_all($this->session->userdata('id'));//panggil ke modell
      $datafield=$this->Things_model->get_field();//panggil ke modell



       if ($this->agent->is_mobile())
       {
         $data = array(
           'contain_view' => 'member/things/things_foo',
           'sidebar'=>'member/sidebar',
           'css'=>'member/crudassets/css_foo',
           'script'=>'member/crudassets/script_foo',
           'datathings'=>$datathings,
           'datafield'=>$datafield,
           'module'=>'member',
           'titlePage'=>'things'
          );
       }else{
         $data = array(
           'contain_view' => 'member/things/things_list',
           'sidebar'=>'member/sidebar',
           'css'=>'member/crudassets/css',
           'script'=>'member/crudassets/script',
           'datathings'=>$datathings,
           'datafield'=>$datafield,
           'module'=>'member',
           'titlePage'=>'things'
          );
       }
      $this->template->load($data);
    }

    public function create(){
      $data = array(
        'contain_view' => 'member/things/things_form',
        'sidebar'=>'member/sidebar',//Ini buat menu yang ditampilkan di module member {DIKIRIM KE TEMPLATE}
        'css'=>'member/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'member/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'member/things/create_action',
        'titlePage'=>'Create Things'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Things_model->get_by_id($id);
      $dataofficial=$this->Things_model->get_all_official();
      $data = array(
        'contain_view' => 'member/things/things_edit',
        'sidebar'=>'member/sidebar',//Ini buat menu yang ditampilkan di module member {DIKIRIM KE TEMPLATE}
        'css'=>'member/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'member/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'member/things/update_action',
        'dataedit'=>$dataedit,
        'dataofficial'=>$dataofficial,
        'titlePage'=>'Edit Things'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'desc' => $this->input->post('desc',TRUE),
		'datetime' => $this->input->post('datetime',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
		'verification' => $this->input->post('verification',TRUE),
		'image' => $this->input->post('image',TRUE),
		'status' => $this->input->post('status',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
	  );

            $this->Things_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('member/things'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id_things', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'desc' => $this->input->post('desc',TRUE),
		//'datetime' => $this->input->post('datetime',TRUE),
		//'latitude' => $this->input->post('latitude',TRUE),
		//'longitude' => $this->input->post('longitude',TRUE),
		'verification' => $this->input->post('verification',TRUE),
		//'image' => $this->input->post('image',TRUE),
		'status' => $this->input->post('status',TRUE),
		//'id_admin' => $this->session->userdata('id'),
		//'id_user' => $this->input->post('id_user',TRUE),
    'id_official_location' => $this->input->post('official',TRUE)
  	);

            $this->Things_model->update($this->input->post('id_things', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('member/things'));
        }
    }

    public function delete($id)
    {
        $row = $this->Things_model->get_by_id($id);

        if ($row) {
            $this->Things_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('member/things'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('member/things'));
        }
    }
    public function broadcastLocation(){
      if(isset($_GET['id']) and isset($_GET['status'])){
        $id=$_GET['id'];
        $status=$_GET['status'];
        if($status=="lost"){
          $status="Hilang";
        }else{
          $status="Temuan";
        }
        $send=new MessageBuilder();
        $image="https://islamify.id/bot/temuan/xfile/images/belum_ada_foto.png";
        $messages=[];
        $msg1=$send->text("[Info System]\nTerdapat Barang $status Didekat Lokasi Anda!");
        $row = $this->Things_model->get_by_id($id);
        if($row){
          $loadImg=$this->Dbs->getdata(array('id_things'=>$row->id_things),'image');
          if($loadImg->num_rows()>0){
            $image='https://islamify.id/bot/temuan/xfile/laporan/'.$loadImg->row()->name;
          }
          $button = array(array('type'=>'postback','label'=>'Lihat Detail','data'=>'detailThings#'.$id.'#'.$status,'text'=>"Lihat"));
          $msg2=$send->buttonMessage($image,"$row->name",substr($row->name,0,30),substr($row->desc,0,59),$button);
          array_push($messages,$msg1,$msg2);
          $getLocationUser=$this->Dbs->getdata(array('id_official_location'=>$row->id_official_location,'id_user!='=>$row->id_user),'circle');
          $this->multicast($getLocationUser,$messages);
          // var_dump($getLocationUser->result());
          $this->session->set_flashdata('message', 'Broadcast Barang Berhasil');
          redirect(site_url('member/things'));
        }else{
          $this->session->set_flashdata('message', 'Record Not Found');
          redirect(site_url('member/things'));
        }
      }else{
        redirect(site_url('member/things'));
      }
    }


    public function broadcast(){
      if(isset($_GET['id']) and isset($_GET['status'])){
        $id=$_GET['id'];
        $status=$_GET['status'];
        if($status=="lost"){
          $status="Hilang";
        }else{
          $status="Temuan";
        }
        $send=new MessageBuilder();
        $image="https://islamify.id/bot/temuan/xfile/images/belum_ada_foto.png";
        $messages=[];
        $msg1=$send->text("[Info System]\nTerdapat Barang $status Didekat Lokasi Anda!");
        $row = $this->Things_model->get_by_id($id);
        if($row){
          $loadImg=$this->Dbs->getdata(array('id_things'=>$row->id_things),'image');
          if($loadImg->num_rows()>0){
            $image='https://islamify.id/bot/temuan/xfile/laporan/'.$loadImg->row()->name;
          }
          $button = array(array('type'=>'postback','label'=>'Lihat Detail','data'=>'detailThings#'.$id.'#'.$status,'text'=>"Lihat"));
          $msg2=$send->buttonMessage($image,"$row->name",substr($row->name,0,30),substr($row->desc,0,59),$button);
          array_push($messages,$msg1,$msg2);
          $getNearUser=$this->Things_model->get_near(3,$row->latitude,$row->longitude,$row->id_user);
          $this->multicast($getNearUser,$messages);
          // var_dump($getNearUser->result());
          $this->session->set_flashdata('message', 'Broadcast Barang Berhasil');
          redirect(site_url('member/things'));
        }else{
          $this->session->set_flashdata('message', 'Record Not Found');
          redirect(site_url('member/things'));
        }
      }else{
        redirect(site_url('member/things'));
      }
    }
    public function verification($id){
      $row = $this->Things_model->get_by_id($id);
      if($row){
        $this->Things_model->verification($id,1);
        $this->verifnotif($row->id_user,$row->name);
        $this->session->set_flashdata('message', 'Verif Record Success');
        redirect(site_url('member/things'));
      }else{
        $this->session->set_flashdata('message', 'Record Not Found');
        redirect(site_url('admin/things'));
      }
    }

    function verifnotif($id_user,$namabarang)
    {
        $channelAccessToken = $this->channelAccessToken;
        $channelSecret = $this->channelSecret;
        $client = new LINEBotTiny($channelAccessToken, $channelSecret);
        $send=new MessageBuilder();
        $messages=[];
          $msg1=$send->text("[Info System]\nLaporan $namabarang anda sudah diverifikasi dan sudah terbit di pencarian!");
          array_push($messages,$msg1);
        $push = array(
          'to' => $id_user,
          'messages' => $messages
        );
        $client->pushMessage($push);
    }

    function multicast($dbModel,$arrayMessage){//ganti kondisi ini
      $channelAccessToken = $this->channelAccessToken;
      $channelSecret = $this->channelSecret;
        $client = new LINEBotTiny($channelAccessToken, $channelSecret);
    		$count=$dbModel->num_rows();

         $get=array_chunk($dbModel->result(),150);
         // var_dump($get);
         foreach ($get as $g) {
           $new=[];
           foreach ($g as $o) {
             array_push($new,$o->id_user);
           }
           $push = array(
                    		'to' => $new,
                    		'messages' => $arrayMessage
                    	);
          // var_dump($push);
                    	$client->multicast($push);

         }

      }

    public function _rules()
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('desc', 'desc', 'trim|required');
	$this->form_validation->set_rules('verification', 'verification', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');


	$this->form_validation->set_rules('id_things', 'id_things', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
