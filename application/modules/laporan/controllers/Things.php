<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Things extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model(array('Dbs','Things_model'));
        $this->load->library('form_validation');
    }

    public function index()
    {
      if(isset($_GET['id'])){
        $id_user=$_GET['id'];
        $checkUser=$this->Dbs->getdata(array('id_user'=>$id_user),'user')->num_rows();
        if($checkUser==0){
          redirect("https://islamify.id");
        }
        $loadDb=$this->Things_model->get_by_iduser($id_user);//panggil ke modell
        $check=$loadDb->num_rows();
        if($check>0){
          $datathings=$loadDb->result();
        }else{
          $datathings=null;
        }
        $datafield=$this->Things_model->get_field();//panggil ke modell
        $data = array(
          'contain_view' => 'laporan/things/things_foo',
          'sidebar'=>'laporan/sidebar',
          'webviewbot'=>'true',
          'css'=>'laporan/crudassets/css_foo',
          'script'=>'laporan/crudassets/script',
          'datathings'=>$datathings,
          'datafield'=>$datafield,
          'module'=>'laporan',
          'titlePage'=>'things'
         );
        $this->template->load($data);

      }else{
        redirect("https://islamify.id");
      }


    }


    public function create(){
      $data = array(
        'contain_view' => 'laporan/things/things_form',
        'sidebar'=>'laporan/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'laporan/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'laporan/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'laporan/things/create_action',
        'titlePage'=>'Tambah data'
       );
      $this->template->load($data);
    }

    public function edit(){
      if(isset($_GET['id']) and isset($_GET['barang'])){
        $id_user=$_GET['id'];
        $id_things=$_GET['barang'];
        $checkUser=$this->Dbs->getdata(array('id_user'=>$id_user),'user')->num_rows();
        if($checkUser==0){
          redirect("https://islamify.id");
        }
        $loadDb=$this->Things_model->get_by_id($id_things);//panggil ke modell
        $check=$loadDb->num_rows();
        if($check>0){
          $dataedit=$loadDb->row();
        }else{
          $datathings=null;
        }
        $username="-";

        if($dataedit->id_official_location!=null){
        $username=$this->Dbs->getdata(array('id_official_location'=>$dataedit->id_official_location),'official_location')->row()->username;
        }
        $getImage=$this->Things_model->getwhere('id_things',$id_things,'image');
        $data = array(
          'contain_view' => 'laporan/things/things_edit',
          'sidebar'=>'laporan/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
          'css'=>'laporan/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
          'script'=>'laporan/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
          'action'=>'laporan/things/update_action',
          'dataedit'=>$dataedit,
          'titlePage'=>'Update Data',
          'webviewbot'=>'true',
          'getImage'=>$getImage,
          'username'=>$username
         );
        $this->template->load($data);

      }else{
        redirect("https://islamify.id");
      }

    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'desc' => $this->input->post('desc',TRUE),
		'datetime' => $this->input->post('datetime',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );
      $sql=$this->Things_model->boolInsert($data);
if($sql){

}
if($_POST['filename'] != ''){
  $id_things=$this->Things_model->getId()->id_things;
  $filename=$this->input->post('filename');
  $arrFilename=explode(",",$filename);
  for($i=0;$i<count($arrFilename);$i++){
    $dataFoto=array(
      'name'=>$arrFilename[$i],
      'id_things'=>$id_things
    );
    $this->Dbs->insert($dataFoto,'image');
  }
}

            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('laporan/things'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id_things', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'desc' => $this->input->post('desc',TRUE),
		'datetime' => $this->input->post('datetime',TRUE),
		'image' => $this->input->post('image',TRUE),
		'status' => $this->input->post('status',TRUE),
	    );

      if(($_POST['filename'] != '')){//pengecekan jika terdapat file foto
  $id_things=$this->input->post('id_things', TRUE);
  $filename=$this->input->post('filename');//mengambil nama file dari input type hidden
  $arrFilename=explode(",",$filename);//dipecah menjadi array
  for($i=0;$i<count($arrFilename);$i++){
    $dataFoto=array(
      'name'=>$arrFilename[$i],
      'id_things'=>$id_things
    );
    $this->Dbs->insert($dataFoto,'image');
  }
}

            $this->Things_model->update($this->input->post('id_things', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('laporan/things/?id='.$this->input->post('id_user',TRUE)));
        }
    }

    public function delete($id)
    {
      if(isset($_GET['id']) and isset($_GET['barang'])){
        $id_user=$_GET['id'];
        $id_things=$_GET['barang'];
        $row = $this->Things_model->get_by_id($id_things)->row();
        if ($row) {
            $this->Things_model->delete($id_things);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('laporan/things/?id='.$id_user));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('laporan/things/?id='.$id_user));
        }
      }else{

      }

    }

    function imageDelete($id){
  $row = $this->Things_model->get_image_by($id);

  if ($row) {
      unlink('xfile/laporan/'.$row->name);//menghapus file
      $this->Things_model->deleteImg($id);//menghapus value di database berdasarkan img_id
      $this->session->set_flashdata('message', 'Delete Sukses');
      redirect($_SERVER['HTTP_REFERER']);
  } else {
      $this->session->set_flashdata('message', 'Record Not Found');
      redirect($_SERVER['HTTP_REFERER']);
  }
}

    public function _rules()
    {

	$this->form_validation->set_rules('id_things', 'id_things', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
