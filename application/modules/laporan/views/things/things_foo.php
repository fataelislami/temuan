<br><br>
<div class="card" style="">
    <div class="card-body">
        <h4 class="card-title">Laporan Anda</h4>
        <h6 class="card-subtitle">Temuan</h6>
        <table id="demo-foo-addrow2" class="table table-bordered table-hover toggle-circle" data-page-size="7">
            <thead>
                <tr>
                    <th data-sort-initial="true" data-toggle="true">Status</th>
                    <th>Barang</th>
                    <th data-hide="phone, tablet">Deskripsi</th>
                    <th data-hide="phone, tablet">Waktu</th>
                    <th data-hide="phone, tablet">Verifikasi</th>
                    <th data-sort-ignore="true" class="min-width">Aksi</th>
                </tr>
            </thead>
            <div class="m-t-40">
                <div class="d-flex">

                        <div class="form-group">
                            <input id="demo-input-search2" type="text" placeholder="Search" autocomplete="off">
                        </div>
                </div>
            </div>
            <?php if($datathings!=null){ ?>
            <tbody>
              <?php foreach ($datathings as $d): ?>
                <tr>
                    <td><span class="label label-table label-success"><?php echo $d->status; ?></span></td>
                    <td><?php echo $d->name; ?></td>
                    <td><?php echo $d->desc ?></td>
                    <td><?php echo $d->datetime ?></td>
                    <td><span class="label label-table label-success"><?php echo $d->verification ?></span> </td>
                    <td>
                      <a href="<?php echo base_url().$module?>/things/edit/?barang=<?php echo $d->id_things ?>&id=<?php echo $d->id_user ?>">
                              <button class="btn btn-success waves-effect waves-light m-r-10">Edit</button>
                          </a>
                          <button  data-toggle="modal" data-target="#responsive-modal" class="btn btn-danger waves-effect waves-light m-r-10 modalDelete" value="<?php echo $d->id_things ?>#<?php echo $d->id_user ?>">Delete</button>

                    </td>
                </tr>
              <?php endforeach; ?>
            </tbody>
          <?php } ?>
            <tfoot>
                <tr>
                    <td colspan="6">
                        <div class="text-right">
                            <ul class="pagination">
                            </ul>
                        </div>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
</div>
<!-- delete modal content -->
<div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Temuan Hi!</h4>
            </div>
            <div class="modal-body">
              <p id="modalMsg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                <a id="modalHref" href="#">
                <button type="button" class="btn btn-danger waves-effect waves-light">Yup!</button>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
