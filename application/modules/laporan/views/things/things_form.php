<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
 } ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Upload Foto Barang</h4>
                <h6 class="card-subtitle">Tarik Gambar atau Pilih Beberapa Gambar</h6>
                <form action="<?php echo base_url()?>laporan/upload/proses" class="dropzone">
                </form>
                <br>
            </div>
        </div>
    </div>
</div>
<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Things</h4>
            <form class="form-material m-t-40" method="post" action="<?php echo base_url().$action ?>">
	  <div class="form-group">
            <label>name</label>
            <input type="text" name="name" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>desc</label>
            <input type="text" name="desc" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>datetime</label>
            <input type="text" name="datetime" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>latitude</label>
            <input type="text" name="latitude" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>longitude</label>
            <input type="text" name="longitude" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>verification</label>
            <input type="text" name="verification" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>image</label>
            <input type="text" name="image" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>status</label>
            <input type="text" name="status" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>id_admin</label>
            <input type="text" name="id_admin" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>id_user</label>
            <input type="text" name="id_user" class="form-control" placeholder="">
    </div>
	  <div class="form-group">
            <label>id_official_location</label>
            <input type="text" name="id_official_location" class="form-control" placeholder="">
    </div>
	    <input type="hidden" name="id_things" />
      <input type="hidden" id="filename" name="filename">

                <div class="form-group">
                  <button id="aplot" type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
