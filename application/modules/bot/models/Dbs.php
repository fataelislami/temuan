<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbs extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }


  function reset($target,$data){
      $this->db->get($target);
      $db=$this->db->update($target,$data);
      if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
  }

  function delete($where,$table)
  {
      $this->db->where($where);
      $this->db->delete($table);
  }

  function add_counter($userid,$counter){
    $this->db->set('counter', $counter+1);
    $this->db->where('id_user',$userid);
    $this->db->update('user');
  }

  function lostfound($id_official_location,$status){//menghitung banyak temuan dan kehilangan
    $sql="SELECT count(`status`) as `total` FROM `things` where `id_official_location`=$id_official_location and `status`='$status'";
    return $this->db->query($sql)->row();
  }

  function logs(){
    $this->db->order_by('id','DESC');
    return $this->db->get('logs_json',1);
  }
  //insert data ke tabel
  function insert($data,$to){
    $insert = $this->db->insert($to, $data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
  }

  function insertlog($data){
    $this->db->insert('logs_json', $data);
  }

  //mengambil berdasarkan data userid
  function getdata($where,$from){
    $this->db->where($where);
    $db=$this->db->get($from);
    return $db;
  }

  function getdataBarang($where,$from,$limit=9,$offset=0){
    $this->db->where($where);
    $this->db->order_by('id_things', 'DESC');
    $db=$this->db->get($from,$limit,$offset);
    return $db;
  }

  function mylocation($userId){
    $sql="SELECT * FROM `user` JOIN `circle` USING (`id_user`) where `id_user`='$userId'";
    return $this->db->query($sql);
  }




  //fungsi untuk mengambil lokasi terdekat berdasarkan longitude latitude di parameter
  function getdistance($kilo,$lat,$lng,$userid,$session1,$session2){
      $this->db->select("*, ( 6371 * acos( cos( radians($lat) ) * cos( radians( cur_lat ) ) * cos( radians( cur_long ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( cur_lat ) ) ) ) AS distance");
        $this->db->having('distance <= ' . $kilo);
        $this->db->order_by('distance');
        $this->db->limit(20, 0);
        $this->db->where('userid !=', $userid);
        $this->db->where('flag !=', $session1);
        $this->db->where('flag !=', $session2);
        $this->db->where('cur_order', null);
        $this->db->where('cur_help', null);
        $db=$this->db->get('donatur');
        return $db;
  }

  function getevent($kilo,$lat,$lng){
      $this->db->select("*, ( 6371 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance");
        $this->db->having('distance <= ' . $kilo);
        $this->db->order_by('distance');
        $this->db->limit(20, 0);
        $db=$this->db->get('event');
        return $db;
  }

  function getnear($kilo,$lat,$lng){
    $this->db->select("*, ( 6371 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance,count(`circle`.`id_user`) as `followers`");
      $this->db->having('distance <= ' . $kilo);
      $this->db->order_by('distance');
      $this->db->limit(20, 0);
      $this->db->from('official_location');
      $this->db->join('circle', 'official_location.id_official_location = circle.id_official_location', 'left');
      $this->db->group_by("circle.id_official_location");
      $db = $this->db->get();
      return $db;
    }


  //fungsi untuk update field berdasarkan userid
  function update($where,$data,$to){
    $this->db->where($where);
    $db=$this->db->update($to,$data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
  }



}
