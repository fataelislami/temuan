<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('ClientAPI.php');
require_once('MessageBuilder.php');
require_once('Flex.php');


class Barang extends Bot{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
  }

  function index()
  {

  }

  function temuan($radius,$lat,$lng,$id_user,$page=1){
    $api=new ClientAPI();
    $send=new MessageBuilder();
    $flex=new Flex();
    $json=$api->get(base_url()."/api/get_near_things?radius=$radius&lat=$lat&lng=$lng&status=found&id_user=$id_user&page=$page");
    $obj=json_decode($json);

    if($obj->status=='success'){//kalo parameter API terisi semua
      if($obj->message=='found'){//jika lokasi baran temuan ditemukan dalam database
        $total=count($obj->results);
        if($total>=10){
            $total=10;
        }else{
            $total=$total;
        }
        $contents=[];
        $limit=1;
        $image=base_url()."xfile/images/belum_ada_foto.png";
        foreach ($obj->results as $o) {
          $title=$o->name;//ini belum dipotong stringnya
          $text=$o->desc;//ini belum dipotong stringnya
          $id_things=$o->id_things;
          $lat_things=$o->latitude;
          $lng_things=$o->longitude;
          $distance=round($o->distance,2)." km";
          $loadImg=$this->Dbs->getdata(array('id_things'=>$id_things),'image');
          if($loadImg->num_rows()>0){
            $image=base_url().'xfile/laporan/'.$loadImg->row()->name;
          }
          $dataUser=$this->Dbs->getdata(array('id_user'=>$o->id_user),'user')->row();
          $item=$flex->itemTemuan($title,$image,$text,$dataUser->name,$dataUser->phone,$id_things,$distance,$lat_things,$lng_things);
          array_push($contents,$item);
          $limit++;
          if($limit>9){
            $item=$flex->next_item("next_near#temuan#$radius#$lat#$lng#".($page+1));
            array_push($contents,$item);
              break;
          }
        }
        $finalcontents=array (
    'type' => 'carousel',
    'contents' =>$contents
  );
        $messages=[];
        $msg1=$send->text("Barang Temuan dalam radius $radius km dari lokasi anda");
        $msg2=$send->flex("Temuan",$finalcontents);
        array_push($messages,$msg1,$msg2);
      }else{//jika barang tidak ada dalam database
        $messages=FALSE;
      }
    }else{//kalo parameter api tidak terisi semua
        $messages=array($send->text("parameter is invalid"));
    }
    return $messages;
  }
  function hilang($radius,$lat,$lng,$id_user,$page=1){
    $api=new ClientAPI();
    $send=new MessageBuilder();
    $flex=new Flex();
    $json=$api->get(base_url()."/api/get_near_things?radius=$radius&lat=$lat&lng=$lng&status=lost&id_user=$id_user&page=$page");
    $obj=json_decode($json);

    if($obj->status=='success'){//kalo parameter API terisi semua
      if($obj->message=='found'){//jika lokasi baran temuan ditemukan dalam database
        $total=count($obj->results);
        if($total>=10){
            $total=10;
        }else{
            $total=$total;
        }
        $contents=[];
        $limit=1;
        $image=base_url()."xfile/images/belum_ada_foto.png";
        foreach ($obj->results as $o) {
          $title=$o->name;//ini belum dipotong stringnya
          $text=$o->desc;//ini belum dipotong stringnya
          $id_things=$o->id_things;
          $lat_things=$o->latitude;
          $lng_things=$o->longitude;
          $distance=round($o->distance,2)." km";
          $loadImg=$this->Dbs->getdata(array('id_things'=>$id_things),'image');
          if($loadImg->num_rows()>0){
            $image=base_url().'xfile/laporan/'.$loadImg->row()->name;
          }
          $dataUser=$this->Dbs->getdata(array('id_user'=>$o->id_user),'user')->row();
          $item=$flex->itemHilang($title,$image,$text,$dataUser->name,$id_things,$distance,$lat_things,$lng_things);
          array_push($contents,$item);
          $limit++;
          if($limit>9){
            $item=$flex->next_item("next_near#hilang#$radius#$lat#$lng#".($page+1));
            array_push($contents,$item);
              break;
          }
        }
        $finalcontents=array (
    'type' => 'carousel',
    'contents' =>$contents
  );
        $messages=[];
        $msg1=$send->text("Barang Hilang dalam radius $radius km dari lokasi anda");
        $msg2=$send->flex("Hilang",$finalcontents);
        array_push($messages,$msg1,$msg2);
      }else{//jika barang tidak ada dalam database
        $messages=FALSE;
      }
    }else{//kalo parameter api tidak terisi semua
        $messages=array($send->text("parameter is invalid"));
    }
    return $messages;
  }



}
