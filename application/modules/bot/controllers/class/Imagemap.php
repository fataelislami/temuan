<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imagemap extends Bot{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

  }

  function cari_barang_temuan(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/cari_barang_temuan_revisi/',
  'altText' => 'Cari barang temuan',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 53,
        'y' => 408,
        'width' => 424,
        'height' => 416,
      ),
      'text' => 'Temuan@Terdekat',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 585,
        'y' => 398,
        'width' => 428,
        'height' => 422,
      ),
      'text' => 'Temuan@Lokasi_saya',
    ),
  ),
);
return $item;
  }

  function cari_barang_hilang(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/cari_barang_hilang_revisi/',
  'altText' => 'Cari barang Hilang',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 53,
        'y' => 408,
        'width' => 424,
        'height' => 416,
      ),
      'text' => 'Hilang@Terdekat',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 585,
        'y' => 398,
        'width' => 428,
        'height' => 422,
      ),
      'text' => 'Hilang@Lokasi_saya',
    ),
  ),
);
return $item;
  }

  function upload_foto(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/upload_foto/',
  'altText' => 'Upload Foto',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'uri',
      'area' =>
      array (
        'x' => 53,
        'y' => 408,
        'width' => 424,
        'height' => 416,
      ),
      'linkUri' => 'line://nv/camera/	',
    ),
    1 =>
    array (
      'type' => 'uri',
      'area' =>
      array (
        'x' => 585,
        'y' => 398,
        'width' => 428,
        'height' => 422,
      ),
      'linkUri' => 'line://nv/cameraRoll/multi',
    ),
  ),
);
return $item;
  }

  function lapor(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/lapor/',
  'altText' => 'Lapor',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 53,
        'y' => 408,
        'width' => 424,
        'height' => 416,
      ),
      'text' => 'Lapor Kehilangan',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 585,
        'y' => 398,
        'width' => 428,
        'height' => 422,
      ),
      'text' => 'Lapor Temuan',
    ),
  ),
);
return $item;
  }

  function sebarin(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/sebarin_revisi/',
  'altText' => 'Sebarin kemana?',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 53,
        'y' => 408,
        'width' => 424,
        'height' => 416,
      ),
      'text' => 'Lokasi Terdekat',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 585,
        'y' => 398,
        'width' => 428,
        'height' => 422,
      ),
      'text' => 'Lokasi Saya',
    ),
  ),
);
return $item;
  }

  function lokasi(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/location/',
  'altText' => 'Kirimkan Lokasi',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'uri',
      'area' =>
      array (
        'x' => 2,
        'y' => 6,
        'width' => 1030,
        'height' => 1022,
      ),
      'linkUri' => 'line://nv/location',
    ),
  ),
);
return $item;
  }

  function tambah_lokasi(){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/tambah_lokasi/',
  'altText' => 'Tambah Lokasi',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 114,
        'y' => 304,
        'width' => 328,
        'height' => 349,
      ),
      'text' => '!username',
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 618,
        'y' => 292,
        'width' => 375,
        'height' => 375,
      ),
      'text' => 'Lokasi Terdekat',
    ),
    2 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 202,
        'y' => 763,
        'width' => 636,
        'height' => 89,
      ),
      'text' => '!daftarin lokasi',
    ),
  ),
);
return $item;
  }

  function profile($userId){
    $item=array (
  'type' => 'imagemap',
  'baseUrl' => base_url().'imagemap/profil/',
  'altText' => 'Profil',
  'baseSize' =>
  array (
    'width' => 1040,
    'height' => 1040,
  ),
  'actions' =>
  array (
    0 =>
    array (
      'type' => 'uri',
      'area' =>
      array (
        'x' => 53,
        'y' => 408,
        'width' => 424,
        'height' => 416,
      ),
      'linkUri' => base_url().'laporan/things/?id='.$userId,
    ),
    1 =>
    array (
      'type' => 'message',
      'area' =>
      array (
        'x' => 585,
        'y' => 398,
        'width' => 428,
        'height' => 422,
      ),
      'text' => 'Lokasi Saya',
    ),
  ),
);
return $item;
  }
}
