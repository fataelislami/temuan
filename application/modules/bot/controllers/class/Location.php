<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('ClientAPI.php');
require_once('MessageBuilder.php');
require_once('Flex.php');


class Location extends Bot{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
  }

  function index()
  {

  }

  function near($latitude,$longitude,$userId){//mengambil lokasi official sekitar
    $api=new ClientAPI();
    $send=new MessageBuilder();
    $flex=new Flex();
    $json=$api->get(base_url()."api/official_location?radius=2&lat=$latitude&lng=$longitude");
    $obj=json_decode($json);
    if($obj->status=='success'){//kalo parameter API terisi semua
      if($obj->total_result>0){
        $contents=[];
        $limit=1;
        $imagesUrl=base_url()."xfile/images/default_lokasi.png";
        foreach ($obj->results as $o) {
              $temuan=$this->Dbs->lostfound($o->id_official_location,"found")->total;
              $kehilangan=$this->Dbs->lostfound($o->id_official_location,"lost")->total;
              $check=$this->Dbs->getdata(array('id_user'=>$userId,'id_official_location'=>$o->id_official_location),'circle')->num_rows();
              if($check>0){
                $status="follow";
              }else{
                $status="unfollow";
              }
              if($o->image!=NULL || $o->image!=''){
                $imagesUrl=base_url()."/assets/official/".$o->image;
              }
              $slide=$flex->nearlocation($o->name,$o->city,$o->followers,$status,$o->username,$temuan,$kehilangan,null,$imagesUrl);
              array_push($contents,$slide);
              if($limit>10){
                break;
              }
              $limit++;
        }

        $finalcontents=array (
    'type' => 'carousel',
    'contents' =>$contents
  );
  $messages=[];
  $msg1=$send->flex("Temuan",$finalcontents);
  array_push($messages,$msg1);
}else{
  $messages=array($send->text("temwan belum menemukan lokasi resmi didekat lokasi mu"));
}
    }else{//kalo parameter api tidak terisi semua
        $messages=array($send->text("parameter is invalid"));
    }
    return $messages;

  }

  function mylocation($id_user){
    $api=new ClientAPI();
    $send=new MessageBuilder();
    $flex=new Flex();
    $json=$api->get(base_url()."/api/mylocation?id_user=$id_user");
    $obj=json_decode($json);
    if($obj->status=='success'){//kalo parameter API terisi semua
      if($obj->total_result>0){
        $contents=[];
        $limit=1;
        $imagesUrl=base_url()."xfile/images/default_lokasi.png";

        foreach ($obj->results as $o) {
              $temuan=$this->Dbs->lostfound($o->id_official_location,"found")->total;
              $kehilangan=$this->Dbs->lostfound($o->id_official_location,"lost")->total;
              if($o->image!=NULL || $o->image!=''){
                $imagesUrl=base_url()."/assets/official/".$o->image;
              }
              $slide=$flex->nearlocation($o->name,$o->city,$o->followers,"follow",$o->username,$temuan,$kehilangan,null,$imagesUrl);
              array_push($contents,$slide);
              if($limit>10){
                break;
              }
              $limit++;
        }

        $finalcontents=array (
    'type' => 'carousel',
    'contents' =>$contents
    );
    $messages=[];
    $msg1=$send->flex("Temuan",$finalcontents);
    array_push($messages,$msg1);
    }else{
    $messages=array($send->text("kamu belum mengikuti lokasi manapun"));
    }
    }else{//kalo parameter api tidak terisi semua
        $messages=array($send->text("parameter is invalid"));
    }
    return $messages;
  }

  function chooselocation($id_user){
    $api=new ClientAPI();
    $send=new MessageBuilder();
    $flex=new Flex();
    $json=$api->get(base_url()."/api/mylocation?id_user=$id_user");
    $obj=json_decode($json);
    if($obj->status=='success'){//kalo parameter API terisi semua
      if($obj->total_result>0){
        $contents=[];
        $limit=1;
        $imagesUrl=base_url()."xfile/images/default_lokasi.png";
        foreach ($obj->results as $o) {
              $temuan=$this->Dbs->lostfound($o->id_official_location,"found")->total;
              $kehilangan=$this->Dbs->lostfound($o->id_official_location,"lost")->total;
              if($o->image!=NULL || $o->image!=''){
                $imagesUrl=base_url()."/assets/official/".$o->image;
              }
              $slide=$flex->nearlocation($o->name,$o->city,$o->followers,"pilih",$o->username,$temuan,$kehilangan,$o->id_official_location,$imagesUrl);
              array_push($contents,$slide);
              if($limit>10){
                break;
              }
              $limit++;
        }

        $finalcontents=array (
    'type' => 'carousel',
    'contents' =>$contents
    );
    $messages=[];
    $msg1=$send->flex("Temuan",$finalcontents);
    array_push($messages,$msg1);
    }else{
    $messages=array($send->text("kamu belum mengikuti lokasi manapun"));
    }
    }else{//kalo parameter api tidak terisi semua
        $messages=array($send->text("parameter is invalid"));
    }
    return $messages;
  }

  function following_by($id_user,$status=null){
    $api=new ClientAPI();
    $send=new MessageBuilder();
    $flex=new Flex();
    $json=$api->get(base_url()."/api/mylocation?id_user=$id_user");
    $obj=json_decode($json);
    if($obj->status=='success'){//kalo parameter API terisi semua
      if($obj->total_result>0){
        $contents=[];
        $limit=1;
        $imagesUrl=base_url()."xfile/images/default_lokasi.png";
        foreach ($obj->results as $o) {
          if($status=='hilang'){
            $labelButton='Cek Barang Hilang';
          }else if($status=='temuan'){
            $labelButton='Cek Barang Temuan';
          }

          if($o->image!=NULL || $o->image!=''){
            $imagesUrl=base_url()."/assets/official/".$o->image;
          }

              $slide=$flex->mylocation($o->name,$o->city,$o->followers,$labelButton,$status,$o->id_official_location,$o->latitude,$o->longitude,$imagesUrl);
              array_push($contents,$slide);
              if($limit>10){
                break;
              }
              $limit++;
        }

        $finalcontents=array (
    'type' => 'carousel',
    'contents' =>$contents
  );
  $messages=[];
  $msg1=$send->flex("Temuan",$finalcontents);
  array_push($messages,$msg1);
      }else{
        $messages=[];
        $msg1=$send->text("Anda Belum Menentukan Lokasi");
        $msg2=$send->text("Silahkan ketik Tambah Lokasi atau cek Menu Tambah Lokasi");
        array_push($messages,$msg1,$msg2);
      }

    }else{//kalo parameter api tidak terisi semua
        $messages=array($send->text("parameter is invalid"));
    }
    return $messages;
  }

  public function temuan($id_official_location,$id_user,$page=1){
    $send=new MessageBuilder();
    $flex=new Flex();
    //ambil data lokasi official_location
    $getLokasi=$this->Dbs->getdata(array('id_official_location'=>$id_official_location),'official_location')->row();
    $latLokasi=$getLokasi->latitude;
    $lngLokasi=$getLokasi->longitude;
    //end ambil data
    $condition="`id_official_location`=$id_official_location AND `status`='found' AND `id_user`<>'$id_user' AND `id_user2` IS NULL";
    $limitDb=9;
    $offsetDb=0;
    if($page!=1 and $page!=0){
      $offsetDb=$limitDb*($page-1);
    }
    $loadDb=$this->Dbs->getdataBarang($condition,'things',$limitDb,$offsetDb);
    $check=$loadDb->num_rows();
    if($check>0){
      $get=$loadDb->result();
      $contents=[];
      $limit=1;
      $image=base_url()."xfile/images/belum_ada_foto.png";
      foreach ($get as $o) {
        $title=$o->name;//ini belum dipotong stringnya
        $text=$o->desc;//ini belum dipotong stringnya
        $id_things=$o->id_things;
        $loadImg=$this->Dbs->getdata(array('id_things'=>$id_things),'image');
        if($loadImg->num_rows()>0){
          $image=base_url().'xfile/laporan/'.$loadImg->row()->name;
        }
        $dataUser=$this->Dbs->getdata(array('id_user'=>$o->id_user),'user')->row();
        $item=$flex->itemTemuan($title,$image,$text,$dataUser->name,$dataUser->phone,$id_things,"-",$latLokasi,$lngLokasi);
        array_push($contents,$item);
        $limit++;
        if($limit>9){
          $item=$flex->next_item("next_barang#temuan#$id_official_location#".($page+1));
          array_push($contents,$item);
            break;
        }
      }
      $finalcontents=array (
  'type' => 'carousel',
  'contents' =>$contents
);
      $messages=[];
      $msg1=$send->text("Barang Temuan di lokasi anda");
      $msg2=$send->flex("Temuan",$finalcontents);
      array_push($messages,$msg1,$msg2);
    }else{
      $messages=array($send->text("Belum ada barang temuan"));
    }

    return $messages;

  }

  public function hilang($id_official_location,$id_user,$page=1){
    $send=new MessageBuilder();
    $flex=new Flex();
    //ambil data lokasi official_location
    $getLokasi=$this->Dbs->getdata(array('id_official_location'=>$id_official_location),'official_location')->row();
    $latLokasi=$getLokasi->latitude;
    $lngLokasi=$getLokasi->longitude;
    //end ambil data
    $condition="`id_official_location`=$id_official_location AND `status`='lost' AND `id_user`<>'$id_user' AND `id_user2` IS NULL";
    $limitDb=9;
    $offsetDb=0;
    if($page!=1 and $page!=0){
      $offsetDb=$limitDb*($page-1);
    }
    $loadDb=$this->Dbs->getdataBarang($condition,'things',$limitDb,$offsetDb);
    $check=$loadDb->num_rows();
    if($check>0){
      $get=$loadDb->result();
      $contents=[];
      $limit=1;
      $image=base_url()."xfile/images/belum_ada_foto.png";
      foreach ($get as $o) {
        $title=$o->name;//ini belum dipotong stringnya
        $text=$o->desc;//ini belum dipotong stringnya
        $id_things=$o->id_things;
        $loadImg=$this->Dbs->getdata(array('id_things'=>$id_things),'image');
        if($loadImg->num_rows()>0){
          $image=base_url().'xfile/laporan/'.$loadImg->row()->name;
        }
        $dataUser=$this->Dbs->getdata(array('id_user'=>$o->id_user),'user')->row();
        $item=$flex->itemHilang($title,$image,$text,$dataUser->name,$id_things,"-",$latLokasi,$lngLokasi);
        array_push($contents,$item);
        $limit++;
        if($limit>9){
          $item=$flex->next_item("next_barang#hilang#$id_official_location#".($page+1));
          array_push($contents,$item);
            break;
        }
      }
      $finalcontents=array (
  'type' => 'carousel',
  'contents' =>$contents
);
      $messages=[];
      $msg1=$send->text("Barang Hilang di lokasi anda");
      $msg2=$send->flex("Temuan",$finalcontents);
      array_push($messages,$msg1,$msg2);
    }else{
      $messages=array($send->text("Belum ada barang hilang"));
    }

    return $messages;

  }

}
