<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Flex extends Bot{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function index()
  {

  }

  function next_item($data){
    $item=array (
  'type' => 'bubble',
  'body' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'sm',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'button',
        'flex' => 1,
        'gravity' => 'center',
        'action' =>
        array (
          'type' => 'postback',
          'data' => $data,
          'label' => 'Next',
          'text'=>'next'
        ),
      ),
    ),
  ),
);
return $item;
  }

  public function nearlocation($name,$city,$followers,$status,$username,$temuan,$kehilangan,$id=null,$gambar){
    if($status=='follow'){
      $button=array (
        'type' => 'message',
        'label' => 'Unfollow',
        'text' => 'unfollow '.$username,
      );
      $color="#FF6B6B";
    }else if($status=='unfollow'){
      $button=array (
        'type' => 'message',
        'label' => 'Follow',
        'text' => 'follow '.$username,
      );
      $color="#4ECDC4";

    }else{
      $button=array (
        'type' => 'postback',
        'data'=>'pilih#'.$id,
        'label' => 'Pilih',
        'text' => 'pilih '.$name,
      );
      $color="#4ECDC4";
    }
    $item=array (
  'type' => 'bubble',
  'hero' =>
  array (
    'type' => 'image',
    'size' => 'full',
    'aspectRatio' => '20:13',
    'aspectMode' => 'cover',
    'url' => $gambar,
  ),
  'body' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'sm',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'text',
        'text' => $name,
        'wrap' => true,
        'weight' => 'bold',
        'size' => 'md',
      ),
      1 =>
      array (
        'type' => 'box',
        'layout' => 'baseline',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'icon',
            'url' => 'https://islamify.id/bot/temuan/assets/icon/location.png',
            'size' => 'md',
          ),
          1 =>
          array (
            'type' => 'text',
            'text' => 'Lokasi',
            'margin' => 'sm',
            'wrap' => true,
            'size' => 'sm',
            'flex' => 1,
          ),
          2 =>
          array (
            'type' => 'text',
            'text' => $city,
            'margin' => 'sm',
            'wrap' => true,
            'size' => 'sm',
            'flex' => 0,
          ),
        ),
      ),
      2 =>
      array (
        'type' => 'box',
        'layout' => 'baseline',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'icon',
            'url' => 'https://islamify.id/bot/temuan/assets/icon/followers.png',
            'size' => 'md',
          ),
          1 =>
          array (
            'type' => 'text',
            'margin' => 'sm',
            'text' => 'Followers',
            'wrap' => true,
            'size' => 'sm',
            'flex' => 1,
          ),
          2 =>
          array (
            'type' => 'text',
            'text' => $followers,
            'margin' => 'sm',
            'wrap' => true,
            'weight' => 'bold',
            'size' => 'sm',
            'flex' => 0,
          ),
        ),
      ),
      3 =>
      array (
        'type' => 'box',
        'layout' => 'horizontal',
        'margin' => 'md',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'text',
            'text' => 'TEMUAN',
            'size' => 'xs',
            'color' => '#aaaaaa',
            'flex' => 0,
          ),
          1 =>
          array (
            'type' => 'text',
            'text' => 'KEHILANGAN',
            'color' => '#aaaaaa',
            'size' => 'xs',
            'align' => 'end',
          ),
        ),
      ),
      4 =>
      array (
        'type' => 'box',
        'layout' => 'horizontal',
        'margin' => 'md',
        'contents' =>
        array (
          0 =>
          array (
            'type' => 'text',
            'text' => $temuan,
            'flex' => 1,
            'gravity' => 'center',
          ),
          1 =>
          array (
            'type' => 'separator',
          ),
          2 =>
          array (
            'type' => 'text',
            'text' => $kehilangan,
            'flex' => 1,
            'gravity' => 'center',
            'align' => 'end',
          ),
        ),
      ),
    ),
  ),
  'footer' =>
  array (
    'type' => 'box',
    'layout' => 'vertical',
    'spacing' => 'md',
    'contents' =>
    array (
      0 =>
      array (
        'type' => 'button',
        'style' => 'primary',
        'action' =>
        $button,
        'color' => $color,
      ),
    ),
  ),
);
    return $item;
  }

  public function mylocation($name,$city,$followers,$labelButton,$status,$id_official_location,$lat,$lng,$imagesUrl){//lokasi yang di follow
    $item=array (
          'type' => 'bubble',
          'hero' =>
          array (
            'type' => 'image',
            'size' => 'full',
            'aspectRatio' => '20:13',
            'aspectMode' => 'cover',
            'url' => $imagesUrl,
          ),
          'body' =>
          array (
            'type' => 'box',
            'layout' => 'vertical',
            'spacing' => 'sm',
            'contents' =>
            array (
              0 =>
              array (
                'type' => 'text',
                'text' => "$name",
                'wrap' => true,
                'weight' => 'bold',
                'size' => 'md',
              ),
              1 =>
              array (
                'type' => 'box',
                'layout' => 'baseline',
                'contents' =>
                array (
                  0 =>
                  array (
                    'type' => 'icon',
                    'url' => 'https://islamify.id/bot/temuan/assets/icon/location.png',
                    'size' => 'md',
                  ),
                  1 =>
                  array (
                    'type' => 'text',
                    'text' => 'Kota',
                    'margin' => 'sm',
                    'wrap' => true,
                    'size' => 'sm',
                    'flex' => 1,
                  ),
                  2 =>
                  array (
                    'type' => 'text',
                    'text' => $city,
                    'margin' => 'sm',
                    'wrap' => true,
                    'size' => 'sm',
                    'flex' => 0,
                  ),
                ),
              ),
              2 =>
              array (
                'type' => 'box',
                'layout' => 'baseline',
                'contents' =>
                array (
                  0 =>
                  array (
                    'type' => 'icon',
                    'url' => 'https://islamify.id/bot/temuan/assets/icon/followers.png',
                    'size' => 'md',
                  ),
                  1 =>
                  array (
                    'type' => 'text',
                    'margin' => 'sm',
                    'text' => 'Followers',
                    'wrap' => true,
                    'size' => 'sm',
                    'flex' => 1,
                  ),
                  2 =>
                  array (
                    'type' => 'text',
                    'text' => $followers,
                    'margin' => 'sm',
                    'wrap' => true,
                    'weight' => 'bold',
                    'size' => 'sm',
                    'flex' => 0,
                  ),
                ),
              ),
            ),
          ),
          'footer' =>
          array (
            'type' => 'box',
            'layout' => 'vertical',
            'spacing' => 'md',
            'contents' =>
            array (
              0 =>
              array (
                'type' => 'button',
                'style' => 'primary',
                'action' =>
                array (
                  'type' => 'postback',
                  'label' => $labelButton,
                  'data' => "cek_barang#$status#".$id_official_location,
                  'text' => 'tap@cek',
                ),
              ),
              1 =>
              array (
                'type' => 'button',
                'style' => 'primary',
                'action' =>
                array (
                  'type' => 'postback',
                  'label' => 'Tuju',
                  'data' => 'goto#'.$lat."#".$lng."#".$name,
                  'text' => 'tuju',
                ),
                'color' => '#4ECDC4',
              ),
            ),
          ),
        );
        return $item;
  }
  public function itemHilang($judul,$urlImage,$desc,$user,$id_things,$distance="-",$lat=00,$lng=00){
    $item=
  array (
    'type' => 'bubble',
    'hero' =>
    array (
      'type' => 'image',
      'size' => 'full',
      'aspectRatio' => '20:13',
      'aspectMode' => 'cover',
      'url' => $urlImage,
    ),
    'body' =>
    array (
      'type' => 'box',
      'layout' => 'vertical',
      'spacing' => 'sm',
      'contents' =>
      array (
        0 =>
        array (
          'type' => 'text',
          'text' => $judul,
          'wrap' => true,
          'weight' => 'bold',
          'size' => 'md',
        ),
        1 =>
        array (
          'type' => 'box',
          'layout' => 'baseline',
          'contents' =>
          array (
            0 =>
            array (
              'type' => 'icon',
              'url' => 'https://islamify.id/bot/temuan/assets/icon/temuaners.png',
              'size' => 'md',
            ),
            1 =>
            array (
              'type' => 'text',
              'margin' => 'sm',
              'text' => 'Temuaners',
              'wrap' => true,
              'size' => 'sm',
              'flex' => 1,
            ),
            2 =>
            array (
              'type' => 'text',
              'text' => $user,
              'margin' => 'sm',
              'wrap' => true,
              'weight' => 'bold',
              'size' => 'sm',
              'flex' => 0,
            ),
          ),
        ),
        2 =>
        array (
          'type' => 'box',
          'layout' => 'baseline',
          'contents' =>
          array (
            0 =>
            array (
              'type' => 'icon',
              'url' => 'https://islamify.id/bot/temuan/assets/icon/distance.png',
              'size' => 'md',
            ),
            1 =>
            array (
              'type' => 'text',
              'margin' => 'sm',
              'text' => 'Jarak',
              'wrap' => true,
              'size' => 'sm',
              'flex' => 1,
            ),
            2 =>
            array (
              'type' => 'text',
              'text' => $distance,
              'margin' => 'sm',
              'wrap' => true,
              'weight' => 'bold',
              'size' => 'sm',
              'flex' => 0,
            ),
          ),
        ),
        3 =>
        array (
          'type' => 'box',
          'layout' => 'baseline',
          'contents' =>
          array (
            0 =>
            array (
              'type' => 'icon',
              'url' => 'https://islamify.id/bot/temuan/assets/icon/desc.png',
              'size' => 'md',
            ),
            1 =>
            array (
              'type' => 'text',
              'text' => substr($desc,0,200),
              'margin' => 'sm',
              'wrap' => true,
              'size' => 'sm',
              'flex' => 0,
            ),
          ),
        ),
      ),
    ),
    'footer' =>
    array (
      'type' => 'box',
      'layout' => 'vertical',
      'spacing' => 'md',
      'contents' =>
      array (
        0 =>
        array (
          'type' => 'button',
          'style' => 'link',
          'action' =>
          array (
            'type' => 'postback',
            'label' => 'SELENGKAPNYA',
            'data' => 'readdesc#'.$id_things,
            'text' => 'lihat selengkapnya',
          ),
        ),
        1 =>
        array (
          'type' => 'button',
          'style' => 'primary',
          'action' =>
          array (
            'type' => 'postback',
            'label' => 'Saya Menemukan Ini',
            'data' => 'claim#lost#'.$id_things,
            'text' => 'Saya Menemukan Ini',
          ),
        ),
        2 =>
        array (
          'type' => 'button',
          'style' => 'primary',
          'action' =>
          array (
            'type' => 'postback',
            'label' => 'Cek Lokasi',
            'data' => 'goto#'.$lat."#".$lng."#Lokasi $judul",
            'text' => 'tuju',
          ),
          'color' => '#4ECDC4',
        ),
      ),
    ),
  );
  return $item;
  }
      public function itemTemuan($judul,$urlImage,$desc,$user,$telepon,$id_things,$distance="-",$lat=00,$lng=00){
        $item=
      array (
        'type' => 'bubble',
        'hero' =>
        array (
          'type' => 'image',
          'size' => 'full',
          'aspectRatio' => '20:13',
          'aspectMode' => 'cover',
          'url' => $urlImage,
        ),
        'body' =>
        array (
          'type' => 'box',
          'layout' => 'vertical',
          'spacing' => 'sm',
          'contents' =>
          array (
            0 =>
            array (
              'type' => 'text',
              'text' => $judul,
              'wrap' => true,
              'weight' => 'bold',
              'size' => 'md',
            ),
            1 =>
            array (
              'type' => 'box',
              'layout' => 'baseline',
              'contents' =>
              array (
                0 =>
                array (
                  'type' => 'icon',
                  'url' => 'https://islamify.id/bot/temuan/assets/icon/temuaners.png',
                  'size' => 'md',
                ),
                1 =>
                array (
                  'type' => 'text',
                  'margin' => 'sm',
                  'text' => 'Temuaners',
                  'wrap' => true,
                  'size' => 'sm',
                  'flex' => 1,
                ),
                2 =>
                array (
                  'type' => 'text',
                  'text' => $user,
                  'margin' => 'sm',
                  'wrap' => true,
                  'weight' => 'bold',
                  'size' => 'sm',
                  'flex' => 0,
                ),
              ),
            ),
            2 =>
            array (
              'type' => 'box',
              'layout' => 'baseline',
              'contents' =>
              array (
                0 =>
                array (
                  'type' => 'icon',
                  'url' => 'https://islamify.id/bot/temuan/assets/icon/distance.png',
                  'size' => 'md',
                ),
                1 =>
                array (
                  'type' => 'text',
                  'margin' => 'sm',
                  'text' => 'Jarak',
                  'wrap' => true,
                  'size' => 'sm',
                  'flex' => 1,
                ),
                2 =>
                array (
                  'type' => 'text',
                  'text' => $distance,
                  'margin' => 'sm',
                  'wrap' => true,
                  'weight' => 'bold',
                  'size' => 'sm',
                  'flex' => 0,
                ),
              ),
            ),
            3 =>
            array (
              'type' => 'box',
              'layout' => 'baseline',
              'contents' =>
              array (
                0 =>
                array (
                  'type' => 'icon',
                  'url' => 'https://islamify.id/bot/temuan/assets/icon/desc.png',
                  'size' => 'md',
                ),
                1 =>
                array (
                  'type' => 'text',
                  'text' => substr($desc,0,200),
                  'margin' => 'sm',
                  'wrap' => true,
                  'size' => 'sm',
                  'flex' => 0,
                ),
              ),
            ),
          ),
        ),
        'footer' =>
        array (
          'type' => 'box',
          'layout' => 'vertical',
          'spacing' => 'md',
          'contents' =>
          array (
            0 =>
            array (
              'type' => 'button',
              'style' => 'link',
              'action' =>
              array (
                'type' => 'postback',
                'label' => 'SELENGKAPNYA',
                'data' => 'readdesc#'.$id_things,
                'text' => 'lihat selengkapnya',
              ),
            ),
            1 =>
            array (
              'type' => 'box',
              'layout' => 'horizontal',
              'spacing' => 'md',
              'contents' =>
              array (
                0 =>
                array (
                  'type' => 'button',
                  'style' => 'primary',
                  'action' =>
                  array (
                    'type' => 'postback',
                    'label' => 'CEK LOKASI',
                    'data' => 'goto#'.$lat."#".$lng."#Lokasi $judul",
                    // 'data' => 'claim#found#'.$id_things,
                    'text' => 'cek tkp',
                  ),
                ),
                1 =>
                array (
                  'type' => 'button',
                  'style' => 'primary',
                  'action' =>
                  array (
                    'type' => 'uri',
                    'label' => 'TELEPON',
                    'uri' => 'tel:'.$telepon,
                  ),
                  'color' => '#FF6B6B',
                ),
              ),
            ),
            2 =>
            array (
              'type' => 'button',
              'style' => 'primary',
              'action' =>
              array (
                'type' => 'postback',
                'label' => 'Ini Barang Saya',
                'data' => 'claim#found#'.$id_things,
                // 'data' => 'goto#'.$lat."#".$lng."#Lokasi $judul",
                'text' => 'ini barang saya',
              ),
              'color' => '#4ECDC4',
            ),
          ),
        ),
      );
      return $item;
      }


}
