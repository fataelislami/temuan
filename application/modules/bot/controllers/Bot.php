<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('line_class.php');
require_once('class/MessageBuilder.php');
require_once('class/Register.php');


class Bot extends MY_Controller {

   /*
        WELCOME TO KOSTLAB X CODEIGNITER FRAMEWORK
        Framework inidibuat untuk memudahkan development chatbot LINE
        Coders : @kostlab @fataelislami

        Dokumentasi Fungsi
                                   function update($where,$data,$to)
                                   function getdata($userid,$from)
                                   function insert($data,$to)

        Struktur Model
            DBS
        Struktur Controller
            Welcome


      Brief Docs
            Set Flag

      Check Flag
      if($db[0]->flag=='blablabla')

      Message Type
      if($message['type']=='location')
      if($message['type']=='text')
      if($message['type']=='image')
      if($message['type']=='audio')
      if($message['type']=='video')

      Event Type
      $event['type'] == 'follow'
      $event['type'] == 'unfollow'
      $event['type'] == 'join'
      $event['type'] == 'leave'




     */

     public function __construct()
          {
            parent::__construct();
            //Codeigniter : Write Less Do More
      $this->load->model(array('Dbs'));


          }


  public function index()
  {

    //Konfigurasi Chatbot
    $channelAccessToken = 'Zp9Sz4p8N5rnosRwDt+CeGDHWa9yD9QAgZGMmFMo/Ehnty8ZUv46FZMlqs42C3C4jsMGzKmcI3SriBE98Dd6aLswgHoH8aNJWUsmC8EGozvfnwDaNNzntIZQy3UMpX1jomdlWSI0jItk6sDLbj2TqgdB04t89/1O/w1cDnyilFU=';
    $channelSecret = '2c0762c35a8b2227bc591505aa4cc833';//sesuaikan

    //Konfigurasi Chatbot END

    $client = new LINEBotTiny($channelAccessToken, $channelSecret);
    $send= new MessageBuilder();
    $reg=new Register();

        $userId   = $client->parseEvents()[0]['source']['userId'];
        $groupId    = $client->parseEvents()[0]['source']['groupId'];
        $replyToken = $client->parseEvents()[0]['replyToken'];
        $timestamp  = $client->parseEvents()[0]['timestamp'];
        $message  = $client->parseEvents()[0]['message'];
        $messageid  = $client->parseEvents()[0]['message']['id'];
        $latitude=$client->parseEvents()[0]['message']['latitude'];
        $longitude=$client->parseEvents()[0]['message']['longitude'];
        $address=$client->parseEvents()[0]['message']['address'];
        $addresstitle=$client->parseEvents()[0]['message']['title'];
        $postback=$client->parseEvents() [0]['postback'];
        $profil = $client->profil($userId);
        $nama=$profil->displayName;
        $pesan_datang = $message['text'];
        $upPesan = strtoupper($pesan_datang);
        $pecahnama=explode(" ",$profil->displayName);
        $namapanggilan=$pecahnama[0];
        $event=$client->parseEvents() [0];

        $db = $this->Dbs->getdata(array('id_user'=>$userId), 'user')->row();
        function getRandom($length = 3) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }

        //Fungsi cek register

        //

            if ($event['type'] == 'follow')//Yang bot lakukan pertama kali saat di add oleh user
              {
                $data=array(
                  'id_user'=>$userId,
                  'name'=>$nama,
                  'flag'=>'register',
                  'counter'=>1
                );
                $sql=$this->Dbs->insert($data,'user');
                if($sql){
                  $buttons=[];
                  $button1 = array('type'=>'postback','label'=>'Cewe','data'=>'gender#female');
                  $button2 = array('type'=>'postback','label'=>'Cowo','data'=>'gender#male');
                  array_push($buttons,$button1,$button2);
                  $messages=[];
                  $msg1=$send->text("Holaa ".$namapanggilan."! sebelum gunain temuan, kenalan dulu kuy! kamu itu cewe apa cowo?");
                  $msg2=$send->confirmMessage("Kenalan cuy!","Jenis Kelamin",$buttons);
                  array_push($messages,$msg1,$msg2);
                  $output = $send->reply($replyToken,$messages);
                }

              }
              if ($event['type'] == 'unfollow')//Yang bot lakukan pertama kali saat di add oleh user
              {

              }

            if ($event['type'] == 'join')
              {

              }


          //MAPPING FITUR
          if($db->flag=='register' and $groupId==null){
            if(substr($postback['data'],0,6)=='gender' && $db->counter==1){
              $arr=explode("#",$postback['data']);
              $gender=$arr[1];
              $data=array(
                'counter'=>$db->counter+1,
                'gender'=>$gender
              );
              $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
              if($sql){
                require_once 'class/Imagemap.php';
                $imagemap=new Imagemap();
                $messages=[];
                $msg1=$send->text('okay! satu tahap lagi nih,untuk menyediakan informasi akurat temuan membutuhkan info lokasi, kirimkan lokasi kamu saat ini ya! tap link ini ');
                $msg2=$imagemap->lokasi();
                array_push($messages,$msg1,$msg2);
                $output=$send->reply($replyToken,$messages);
              }
            }else if($message['type']=='text' and $db->counter==1){
              //$messages=array($send->text("Ups kamu belum mengatur jenis kelamin mu, silahkan tap button jenis kelamin untuk menyelesaikan registrasi"));
              $buttons=[];
              $button1 = array('type'=>'postback','label'=>'Cewe','data'=>'gender#female');
              $button2 = array('type'=>'postback','label'=>'Cowo','data'=>'gender#male');
              array_push($buttons,$button1,$button2);
              $messages=[];
              $msg1=$send->text("Ups kamu belum mengatur jenis kelamin mu, silahkan tap button jenis kelamin untuk menyelesaikan registrasi");
              $msg2=$send->confirmMessage("Kenalan cuy!","Jenis Kelamin",$buttons);
              array_push($messages,$msg1,$msg2);
              $output = $send->reply($replyToken,$messages);
            }
            if($message['type']=='location' && $db->counter==2){
              require_once 'class/LocationHelper.php';
              $location=new LocationHelper();
              $city=$location->getCity($latitude,$longitude);
              $data=array(
                'city'=>$city,
                'latitude'=>$latitude,
                'longitude'=>$longitude,
                'counter'=>0,
                'flag'=>'default'
              );
              $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
              if($sql){
                $messages=[];
                $msg1=$send->text('Okay! temuan udh kenal kamu nih!, kalo masi bingung silahkan cek Timeline aja yak!');
                $msg2=$send->imagemapurl(base_url().'imagemap/welcome/',"Selamat Menggunakan","http://line.me/R/home/public/post?id=cyd4818y&postId=1153861253906066431");
                array_push($messages,$msg1,$msg2);
                $output=$send->reply($replyToken,$messages);
              }

            }else if($message['type']=='text' and $db->counter==2){
              $messages=array($send->text("Ups kamu belum memberitahu lokasimu, silahkan tap button lokasi untuk menyelesaikan registrasi, atau kirim lokasi secara manual"));
              $output=$send->reply($replyToken,$messages);
            }

          }
          else if($db->flag=='locationUpdate'){
            if($message['type']=='location'){
              require_once 'class/LocationHelper.php';
              $location=new LocationHelper();
              $city=$location->getCity($latitude,$longitude);
              $data=array(
                'city'=>$city,
                'latitude'=>$latitude,
                'longitude'=>$longitude,
                'counter'=>0,
                'flag'=>'default'
              );
              $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
              if($sql){
                $messages=[];
                $msg1=$send->text('lokasi berhasil di update');
                array_push($messages,$msg1);
                $output=$send->reply($replyToken,$messages);
              }

            }else if($message['type']=='text'){
              $messages=array($send->text("Ups kamu belum memberi tahu lokasi terbaru, silahkan tap button lokasi untuk menggunakan bot kembali"));
              $output=$send->reply($replyToken,$messages);
            }
          }
          else if (substr($db->flag,0,4) == 'chat')//'flag'=>'chat#'.$checkstatus->id_user.'#'.$id_things,
            	{
              //if postback di reset
              if($postback['data']=='reset'){
                $data = array(
            			'chat' => null,
            			'flag' => 'default',
            		);
                $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
            		if ($sql)
            			{
            			$data = array(
            				'chat' => null,
            				'flag' => 'default',
            			);
                  $sql=$this->Dbs->update(array('id_user'=>$db->chat),$data,'user');
            			if ($sql)
            				{
            				$ballons=array($send->text("Bot telah di reset"));//Untuk reset
                    $output=$send->reply($replyToken,$ballons);
            				$push = array(
            					'to' => $db->chat,
            					'messages' => array(
            						array(
            							'type' => 'text',
            							'text' => 'lawan chat meninggalkan obrolan'
            						)
            					)
            				);
            				$client->pushMessage($push,$channelAccessTokenTarget);
            				}
            			}
              }else if($postback['data']=='finish'){
                $getflag=explode("#",$db->flag);
                $id_userThings=$getflag[1];
                $idThings=$getflag[2];
                if($userId==$id_userThings){//jika user id yang mengakhiri sama dengan user id yang memasang laporan barang
                  $set['id_user2']=$db->chat;
                  $set['visible']='no';
                  $sqlThings=$this->Dbs->update(array('id_things'=>$idThings),$set,'things');
                  if($sqlThings){
                    $data = array(
                			'chat' => null,
                			'flag' => 'default',
                		);
                    $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                		if ($sql)
                			{
                			$data = array(
                				'chat' => null,
                				'flag' => 'default',
                			);
                      $sql=$this->Dbs->update(array('id_user'=>$db->chat),$data,'user');
                			if ($sql)
                				{
                				$ballons=array($send->text("Terimakasih!,diskusi selesai dan barang sudah tidak muncul di temuan"));//Untuk reset
                        $output=$send->reply($replyToken,$ballons);
                				$push = array(
                					'to' => $db->chat,
                					'messages' => array(
                						array(
                							'type' => 'text',
                							'text' => 'Terimakasih!,diskusi selesai dan barang sudah tidak muncul di temuan'
                						)
                					)
                				);
                				$client->pushMessage($push,$channelAccessTokenTarget);
                				}
                			}
                  }
                }else{
                  $message=array($send->text("Kamu tidak punya akses untuk menentukan status selesai dari diskusi chat laporan ini"));
                  $output=$send->reply($replyToken,$message);
                }

              }
              //if postback di reset

            	if (stripos($upPesan, 'RESET') !== false or stripos($upPesan, 'KELUAR') !== false)
            		{
                  $messages=[];
                  $items=[];
                  $action1=$send->quickAction(array('type'=>'postback','label'=>'Ya','data'=>'reset'));
                  $action2=$send->quickAction(array('type'=>'postback','label'=>'Tidak','data'=>'noreset'));//tidak diterima dimana mana
                  array_push($items,$action1,$action2);
                  $msg1=$send->quickReply("Apakah anda yakin ingin keluar dari obrolan?",$items);
                  array_push($messages,$msg1);
                  $output = $send->reply($replyToken, $messages);
            		} else if(stripos($upPesan, 'BERES') !== false or stripos($upPesan, 'SELESAI') !== false){
                  $messages=[];
                  $items=[];
                  $action1=$send->quickAction(array('type'=>'postback','label'=>'Ya','data'=>'finish'));
                  $action2=$send->quickAction(array('type'=>'postback','label'=>'Belum','data'=>'nofinish'));//tidak diterima dimana mana
                  array_push($items,$action1,$action2);
                  $msg1=$send->quickReply("Apakah diskusi terkait barang yang anda maksud sudah selesai?",$items);
                  array_push($messages,$msg1);
                  $output = $send->reply($replyToken, $messages);
                }
            	  else
            		{
            		$push = array(
            			'to' => $db->chat,
            			'messages' => array(
            				array(
            					'type' => 'text',
            					'text' => $namapanggilan . ' : ' . $pesan_datang
            				)
            			)
            		);
            		$client->pushMessage($push,$channelAccessTokenTarget);
            		}
            	}

          else if($db->flag=='phoneUpdate'){
            if($message['type']=='text'){
              $phone=preg_replace('/[^0-9]/', '', $pesan_datang);
              $data['phone']=$phone;
              if($phone!=0){
                $data['flag']='default';
                $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                if($sql){
                  $message=array($send->text("Nomor telfon berhasil di simpan, silahkan ulangi perintah"));
                  $output=$send->reply($replyToken,$message);
                }
              }
            }

          }
          else if($db->flag=='lost'){
            if($message['type']=='location'){
              require_once 'class/Barang.php';
              $barang=new Barang();
              $data['flag']='default';
              $radius=3;
              $this->Dbs->update(array('id_user'=>$userId),$data,'user');
              $message=$barang->hilang($radius,$latitude,$longitude,$userId);
              if($message===FALSE){
                $message=[];
                $items=[];
                $action1=$send->quickAction(array('type'=>'postback','label'=>'+5 KM','data'=>"add_radius#hilang#5#$latitude#$longitude",'text'=>'perbesar jadi 5 kilometer dari saya'));
                $action2=$send->quickAction(array('type'=>'postback','label'=>'+10 KM','data'=>"add_radius#hilang#10#$latitude#$longitude",'text'=>'perbesar jadi 10 kilometer dari saya'));
                $action3=$send->quickAction(array('type'=>'postback','label'=>'+15 KM','data'=>"add_radius#hilang#15#$latitude#$longitude",'text'=>'perbesar jadi 15 kilometer dari saya'));
                $action4=$send->quickAction(array('type'=>'postback','label'=>'+20 KM','data'=>"add_radius#hilang#20#$latitude#$longitude",'text'=>'perbesar jadi 20 kilometer dari saya'));
                $action5=$send->quickAction(array('type'=>'postback','label'=>'+25 KM','data'=>"add_radius#hilang#25#$latitude#$longitude",'text'=>'perbesar jadi 25 kilometer dari saya'));
                array_push($items,$action1,$action2,$action3,$action4,$action5);
                $msg1=$send->text("Ups! temuan tidak menemukan barang hilang dalam radius $radius km dari kamu");
                $msg2=$send->quickReply("Apakah kamu ingin memperbesar jarak pencarian? silahkan pilih radius yang diinginkan",$items);
                array_push($message,$msg1,$msg2);
              }
              $output=$send->reply($replyToken,$message);
            }else{
              $data['flag']='default';
              $this->Dbs->update(array('id_user'=>$userId),$data,'user');
            }
          }else if($db->flag=='found'){
            if($message['type']=='location'){
              require_once 'class/Barang.php';
              $barang=new Barang();
              $data['flag']='default';
              $radius=3;
              $this->Dbs->update(array('id_user'=>$userId),$data,'user');
              $message=$barang->temuan($radius,$latitude,$longitude,$userId);
              if($message===FALSE){//
                $message=[];
                $items=[];
                $action1=$send->quickAction(array('type'=>'postback','label'=>'+5 KM','data'=>"add_radius#temuan#5#$latitude#$longitude",'text'=>'perbesar jadi 5 kilometer dari saya'));
                $action2=$send->quickAction(array('type'=>'postback','label'=>'+10 KM','data'=>"add_radius#temuan#10#$latitude#$longitude",'text'=>'perbesar jadi 10 kilometer dari saya'));
                $action3=$send->quickAction(array('type'=>'postback','label'=>'+15 KM','data'=>"add_radius#temuan#15#$latitude#$longitude",'text'=>'perbesar jadi 15 kilometer dari saya'));
                $action4=$send->quickAction(array('type'=>'postback','label'=>'+20 KM','data'=>"add_radius#temuan#20#$latitude#$longitude",'text'=>'perbesar jadi 20 kilometer dari saya'));
                $action5=$send->quickAction(array('type'=>'postback','label'=>'+25 KM','data'=>"add_radius#temuan#25#$latitude#$longitude",'text'=>'perbesar jadi 25 kilometer dari saya'));
                array_push($items,$action1,$action2,$action3,$action4,$action5);
                $msg1=$send->text("Ups! sepertinya belum ada barang temuan dalam radius $radius km dari kamu");
                $msg2=$send->quickReply("Apakah kamu ingin memperbesar jarak pencarian? silahkan pilih radius yang diinginkan",$items);
                array_push($message,$msg1,$msg2);
              }
              $output=$send->reply($replyToken,$message);
            }else{
              $data['flag']='default';
              $this->Dbs->update(array('id_user'=>$userId),$data,'user');
            }
          }
          else if($db->flag=='nearlocation'){
            if($message['type']=='location'){
              require_once 'class/Location.php';
              $location=new Location();
              $data['flag']='default';
              $this->Dbs->update(array('id_user'=>$userId),$data,'user');
              $message=$location->near($latitude,$longitude,$userId);
              $output=$send->reply($replyToken,$message);
            }else{
              $data['flag']='default';
              $this->Dbs->update(array('id_user'=>$userId),$data,'user');
            }
          }
          else if(substr($db->flag,0,6)=='report'){
            $getpostdata=explode("#",$db->flag);
            $id_things=$getpostdata[2];
            $status=$getpostdata[1];
            $condition=array('id_user'=>$userId,'id_things'=>$id_things);
            if($message['type']=='text' and $db->counter==1){
              $data['name']=$pesan_datang;
              $sql=$this->Dbs->update($condition,$data,'things');
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $message=array($send->text("Oke! bisa jelaskan bagaimana deskripsi barang tersebut?"));
                $output=$send->reply($replyToken,$message);
              }
            }
            if($message['type']=='text' and $db->counter==2){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $data['desc']=$pesan_datang;
              $sql=$this->Dbs->update($condition,$data,'things');
              if($sql){
                $this->Dbs->add_counter($userId,$db->counter);
                $messages=[];
                $msg1=$send->text("untuk mempermudah temuan, silakan sertakan foto barang tersebut");
                $msg2=$imagemap->upload_foto();
                array_push($messages,$msg1,$msg2);
                $output=$send->reply($replyToken,$messages);
              }
            }

            if ($message['type']=='image' and $db->counter==3){
              $imgprocess=$this->getMessage($messageid);
              if($imgprocess!="error"){
                $data['name']=$imgprocess;
                $data['id_things']=$id_things;

                $sql=$this->Dbs->insert($data,'image');
                if($sql){
                  $this->Dbs->add_counter($userId,$db->counter);
                  $buttons = [];
                  $button1 = array(
                    'type' => 'datetimepicker',
                    'label' => 'Tentukan Waktu',
                    'data' => 'datetime',
                    'mode' => 'datetime',
                    'initial' => date("Y-m-d").'t00:00',
                    'max' => '2100-12-31T23:59',
                    'min' => '1900-01-01T00:00',
                    'text' => '@TAPTIME'
                  );
                  array_push($buttons, $button1);
                  $messages = [];
                  if($status=='lost'){
                    $msg1 = $send->text('foto sudah disimpan!,btw ilangnya kapan nih?');
                  }else{
                    $msg1 = $send->text('foto sudah disimpan!,btw barangnya ketemu pas kapan nih?');
                  }
                  $msg2 = $send->buttonMessage(base_url()."xfile/images/kalender.png", "kapan?", "Kapan?", "klik button dibawah ini", $buttons);
                  array_push($messages, $msg1, $msg2);
                  $output = $send->reply($replyToken, $messages);
                }
              }else{
                $pre=array($send->text("Gagal Upload,Pastikan Ukuran Foto Tidak Terlalu Besar"));
                $output=$send->reply($replyToken,$pre);
              }
            }else if($db->counter==3){
              $message=array($send->text("Ups upload dulu minimal 1 Foto untuk melanjutkan laporan"));
              $output=$send->reply($replyToken,$message);
            }

            if ($postback['data'] == 'datetime' and $db->counter==4)
                {
                  require_once('class/Imagemap.php');
                  $imagemap=new Imagemap();
                $datapos = explode("T",$postback['params']['datetime']);
                $datetime=$datapos[0]." ".$datapos[1].":00";
                $data['datetime']=$datetime;
                $sql=$this->Dbs->update($condition,$data,'things');
                if($sql){
                  $this->Dbs->add_counter($userId,$db->counter);
                  $ballons=[];
                  $ballon1=$send->text("Waktu berhasil dicatat!");
                  $ballon2=$imagemap->sebarin();
                  array_push($ballons,$ballon1,$ballon2);
                  $output = $send->reply($replyToken,$ballons);
                }

              }else if($db->counter==4){
                  $message=array($send->text("Ups kamu belum menentukan kapan waktu terjadinya"));
                  $output=$send->reply($replyToken,$message);
                }

                if($upPesan=="LOKASI TERDEKAT"){
                  require_once('class/Imagemap.php');
                  $imagemap=new Imagemap();
                  $messages=array($imagemap->lokasi());
                  $output=$send->reply($replyToken,$messages);
                }else
                if($upPesan=="LOKASI SAYA"){
                  require_once 'class/Location.php';
                  $location = new Location();
                  $messages=$location->chooselocation($userId);
                  $output=$send->reply($replyToken,$messages);
                }else if($message['type']=='text' and $db->counter==5 and substr($upPesan,0,5)!='PILIH'){
                  $message=array($send->text("Ups! silahkan tentukan target lokasi terlebih dahulu untuk menyelesaikan laporan"));
                  $output=$send->reply($replyToken,$message);
                }
                //LOKASI
                if(substr($postback['data'],0,5)=="pilih" and $db->counter==5){
                  $datapos=explode("#",$postback['data']);
                  $id_official_location=$datapos[1];
                  $data['id_official_location']=$id_official_location;
                  $data['latitude']=null;
                  $data['longitude']=null;
                  $sql=$this->Dbs->update($condition,$data,'things');
                  if($sql){
                    $set['flag']='default';
                    $set['counter']=0;
                    $this->Dbs->update(array('id_user'=>$userId),$set,'user');
                    $messages=[];
                    $msg1=$send->text("Laporan berhasil tercatat dan akan terpublish setelah diverifikasi admin,temuan akan membantu kamu sebisa mungkin,Semangat!");
                    $msg2=$send->text("Untuk melihat laporan anda silahkan ketik PROFIL atau cek Menu Profil");
                    array_push($messages,$msg1,$msg2);
                    $output=$send->reply($replyToken,$messages);
                    //
                    $loadDb=$this->Dbs->getdata(array('id_official_location'=>$id_official_location),'official_location');
                    $get=$loadDb->row();
                    $msgPush=[];
                    $button = array(array('type'=>'uri','label'=>'Lihat Data','uri'=>base_url().'login/line_auth/'.$get->id_user));
                    $msgPush1=$send->buttonMessage("https://islamify.id/bot/temuan/xfile/partnership.png","Ada Satu Laporan Menunggu Verifikasi","Notifikasi","Ada Satu Laporan Menunggu Verifikasi",$button);
                    array_push($msgPush,$msgPush1);
                    $push = array(
                      'to' => $get->id_user,
                      'messages' => $msgPush
                    );
                    $client->pushMessage($push);
                  }
                }
                if($message['type']=='location' and $db->counter==5){
                  $data['latitude']=$latitude;
                  $data['longitude']=$longitude;
                  $data['id_official_location']=null;

                  $sql=$this->Dbs->update($condition,$data,'things');
                  if($sql){
                    $set['flag']='default';
                    $set['counter']=0;
                    $this->Dbs->update(array('id_user'=>$userId),$set,'user');
                    $messages=[];
                    $msg1=$send->text("Laporan berhasil tercatat dan akan terpublish setelah diverifikasi admin,temuan akan membantu kamu sebisa mungkin,Semangat!");
                    $msg2=$send->text("Untuk melihat laporan anda silahkan ketik PROFIL atau cek Menu Profil");
                    array_push($messages,$msg1,$msg2);
                    $output=$send->reply($replyToken,$messages);
                    //
                    $msgPush=[];
                    $button = array(array('type'=>'uri','label'=>'Lihat Data','uri'=>base_url().'login'));
                    $msgPush1=$send->buttonMessage("https://islamify.id/bot/temuan/xfile/partnership.png","Ada Satu Laporan Menunggu Verifikasi","Notifikasi","Ada Satu Laporan Menunggu Verifikasi",$button);
                    array_push($msgPush,$msgPush1);
                    $push = array(
                      'to' => 'U1b259cb6fde63582365df5e3ad68c543',
                      'messages' => $msgPush
                    );
                    $client->pushMessage($push);
                  }
                }

                //LOKASI
          }
          else{

            //BARANG HILANG
            if($upPesan=="HILANG@TERDEKAT"){
              $data['flag']='lost';
              $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
              if($sql){
                require_once('class/Imagemap.php');
                $imagemap=new Imagemap();
                $messages=array($imagemap->lokasi());
                $output=$send->reply($replyToken,$messages);
              }
            }

            if($upPesan=="HILANG@LOKASI_SAYA"){
              require_once 'class/Location.php';
              $location = new Location();
              $messages=$location->following_by($userId,"hilang");
              $output=$send->reply($replyToken,$messages);
            }
            //BARANG HILANG end

            //BARANG Temuan
            if($upPesan=="TEMUAN@TERDEKAT"){
              $data['flag']='found';
              $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
              if($sql){
                require_once('class/Imagemap.php');
                $imagemap=new Imagemap();
                $messages=array($imagemap->lokasi());
                $output=$send->reply($replyToken,$messages);
              }
            }

            if($upPesan=="TEMUAN@LOKASI_SAYA"){
              require_once 'class/Location.php';
              $location = new Location();
              $messages=$location->following_by($userId,"temuan");
              $output=$send->reply($replyToken,$messages);
            }
            //BARANG TEMUAN END
            if($upPesan=="@UPDATE"){
              require_once 'class/Imagemap.php';
              $imagemap=new Imagemap();
              $data['flag']='locationUpdate';
              $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
              if($sql){
                $messages=[];
                $msg1=$send->text("Silahkan Kirimkan Lokasi Terbaru");
                $msg2=$imagemap->lokasi();
                array_push($messages,$msg1,$msg2);
                $output=$send->reply($replyToken,$messages);
                $client->replyMessage($output);
              }
            }
            //Lapor HILANG
            if($upPesan=="LAPOR KEHILANGAN"){
              if($db->phone==null){
                $data['flag']='phoneUpdate';
                $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                if($sql){
                  $message=array($send->text("Ups! kamu belum mengatur nomor yang bisa dihubungi, silahkan balas bot ini dengan nomor telfon kamu"));
                  $output=$send->reply($replyToken,$message);
                  $client->replyMessage($output);
                }
              }else{
                $dataThings=array(
                  'status'=>'lost',
                  'id_user'=>$userId,
                );
                $sql=$this->Dbs->insert($dataThings,'things');
                if($sql){
                  $recent_id=$this->db->insert_id();//id yang digenerate oleh Auto Increment
                  $data['flag']='report#lost#'.$recent_id;
                  $data['counter']=1;
                  $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                  if($sql){
                    $message=array($send->text("Kehilangan Barang apa nih?"));
                    $output=$send->reply($replyToken,$message);
                  }
                }
              }

            }
            //Lapor HILANG
            //Lapor Temuan
            if($upPesan=="LAPOR TEMUAN"){
              if($db->phone==null){
                $data['flag']='phoneUpdate';
                $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                if($sql){
                  $message=array($send->text("Ups! kamu belum mengatur nomor yang bisa dihubungi, silahkan balas bot ini dengan nomor telfon kamu"));
                  $output=$send->reply($replyToken,$message);
                  $client->replyMessage($output);
                }
              }else{
                $dataThings=array(
                  'status'=>'found',
                  'id_user'=>$userId,
                );
                $sql=$this->Dbs->insert($dataThings,'things');
                if($sql){
                  $recent_id=$this->db->insert_id();//id yang digenerate oleh Auto Increment
                  $data['flag']='report#found#'.$recent_id;
                  $data['counter']=1;
                  $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                  if($sql){
                    $message=array($send->text("Menemukan Barang apa nih?"));
                    $output=$send->reply($replyToken,$message);
                  }
                }
              }

            }
            //LAPOR TEMUAN

            //Sistem Follow
            if (substr($upPesan,0,6)=="FOLLOW"){
              $exData=explode(" ",$pesan_datang);
              $username=$exData[1];
              $condition=array('username'=>$username);
              $loadDb=$this->Dbs->getdata($condition,'official_location');
              $check=$loadDb->num_rows();
              if($check>0){//jika username lokasi ditemukan
                $get=$loadDb->row();
                $condition2=array('id_user'=>$userId,'id_official_location'=>$get->id_official_location);
                $loadDb2=$this->Dbs->getdata($condition2,'circle');
                $check2=$loadDb2->num_rows();
                if($check2>0){
                  $messages=[];
                  $msg1=$send->text("Anda sudah mengikuti lokasi ini");
                  array_push($messages,$msg1);
                  $output=$send->reply($replyToken,$messages);
                }else{
                  $data=array('id_user'=>$userId,'id_official_location'=>$get->id_official_location);
                  $sql=$this->Dbs->insert($data,'circle');
                  if($sql){
                    $messages=[];
                    $msg1=$send->text("Lokasi berhasil diikuti");
                    array_push($messages,$msg1);
                    $output=$send->reply($replyToken,$messages);
                  }
                }
                $output=$send->reply($replyToken,$messages);
              }else{//jika username lokasi tidak ditemukan
                $messages=[];
                $msg1=$send->text("Username tidak ditemukan");
                array_push($messages,$msg1);
                $output=$send->reply($replyToken,$messages);

              }

            }
            //Sistem Follow
            //Sistem unFollow
            if (substr($upPesan,0,8)=="UNFOLLOW"){
              $exData=explode(" ",$pesan_datang);
              $username=$exData[1];
              $condition=array('username'=>$username);
              $loadDb=$this->Dbs->getdata($condition,'official_location');
              $check=$loadDb->num_rows();
              if($check>0){//jika username lokasi ditemukan
                $get=$loadDb->row();
                $condition2=array('id_user'=>$userId,'id_official_location'=>$get->id_official_location);
                $loadDb2=$this->Dbs->getdata($condition2,'circle');
                $check2=$loadDb2->num_rows();
                if($check2>0){
                  $data=array('id_user'=>$userId,'id_official_location'=>$get->id_official_location);
                  $this->Dbs->delete($data,'circle');
                    $messages=[];
                    $msg1=$send->text("Unfollow berhasil");
                    array_push($messages,$msg1);
                    $output=$send->reply($replyToken,$messages);
                }else{
                  //
                  $messages=[];
                  $msg1=$send->text("Anda belum mengikuti lokasi ini");
                  array_push($messages,$msg1);
                  $output=$send->reply($replyToken,$messages);
                  //

                }
                $output=$send->reply($replyToken,$messages);
              }else{//jika username lokasi tidak ditemukan
                $messages=[];
                $msg1=$send->text("Username tidak ditemukan");
                array_push($messages,$msg1);
                $output=$send->reply($replyToken,$messages);

              }

            }
            //Sistem unFollow



            //RichMenu Receiver
            if (substr($upPesan,0,6)=='BARANG'){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $keyword=explode(" ",$upPesan);
              if($keyword[1]=='HILANG'){
                $message=array($imagemap->cari_barang_hilang());
                $output=$send->reply($replyToken,$message);
              }else if($keyword[1]=='TEMUAN'){
                $message=array($imagemap->cari_barang_temuan());
                $output=$send->reply($replyToken,$message);
              }
            }
            if ($upPesan=='LAPOR'){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $message=array($imagemap->lapor());
              $output=$send->reply($replyToken,$message);
            }
            //Lokasi yang difollow
            if($upPesan=="LOKASI SAYA"){
              require_once 'class/Location.php';
              $location = new Location();
              $messages=$location->mylocation($userId);
              $output=$send->reply($replyToken,$messages);
            }
            //Lokasi yang difollow
            //Search Near Location
            if ($upPesan=='LOKASI TERDEKAT'){
              $data['flag']='nearlocation';
              $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
              if($sql){
                require_once('class/Imagemap.php');
                $imagemap=new Imagemap();
                $messages=array($imagemap->lokasi());
                $output=$send->reply($replyToken,$messages);
              }
            }
            //Search Near Location
            //Tambah LOKASI
            if($upPesan=="TAMBAH LOKASI"){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $messages=[];
              $msg1=$send->text("Pilih metode tambah lokasi dibawah ini");
              $msg2=$imagemap->tambah_lokasi();
              array_push($messages,$msg1,$msg2);
              $output=$send->reply($replyToken,$messages);
            }
            //Tambah Lokasi
            //profil
            if($upPesan=="PROFIL"){
              require_once('class/Imagemap.php');
              $imagemap=new Imagemap();
              $messages=array($imagemap->profile($userId));
              $output=$send->reply($replyToken,$messages);
            }
            //Rich menu Receiver

            //POSTBACK UNTUK MENERIMA LOKASI
            if (substr($postback['data'],0,4)=='goto'){
                $getpostdata=explode("#",$postback['data']);
                $address=$getpostdata[3];
                $lat=$getpostdata[1];
                $long=$getpostdata[2];
                $pre=array($send->location("Tap untuk melihat lokasi",$address,$lat,$long));
                $output=$send->reply($replyToken,$pre);
            }
            if (substr($postback['data'],0,12)=='detailThings'){
              require_once('class/Flex.php');
              $flex=new Flex();
              $image=base_url()."xfile/images/belum_ada_foto.png";
              $getpostdata=explode("#",$postback['data']);
              $id=$getpostdata[1];
              $status=$getpostdata[2];
              $loadDb=$this->Dbs->getdata(array('id_things'=>$id),'things');
              $o=$loadDb->row();
              $title=$o->name;//ini belum dipotong stringnya
              $text=$o->desc;//ini belum dipotong stringnya
              $id_things=$o->id_things;
              $lat_things=$o->latitude;
              $lng_things=$o->longitude;
              $distance=round($o->distance,2)." km";
              $loadImg=$this->Dbs->getdata(array('id_things'=>$id_things),'image');
              if($loadImg->num_rows()>0){
                $image=base_url().'xfile/laporan/'.$loadImg->row()->name;
              }
              if($status=="Temuan"){
                $messages=[];
                $dataUser=$this->Dbs->getdata(array('id_user'=>$o->id_user),'user')->row();
                $item=$flex->itemTemuan($title,$image,$text,$dataUser->name,$dataUser->phone,$id_things,$distance,$lat_things,$lng_things);
                $msg1=$send->text("Barang Temuan");
                $msg2=$send->flex("Barang Temuan",$item);
                array_push($messages,$msg1,$msg2);
              }else{
                $messages=[];
                $dataUser=$this->Dbs->getdata(array('id_user'=>$o->id_user),'user')->row();
                $item=$flex->itemHilang($title,$image,$text,$dataUser->name,$id_things,$distance,$lat_things,$lng_things);
                $msg1=$send->text("Barang Hilang");
                $msg2=$send->flex("Barang Hilang",$item);
                array_push($messages,$msg1,$msg2);
              }
              $output=$send->reply($replyToken,$messages);

            }
            if (substr($postback['data'],0,8)=='readdesc'){
              $getpostdata=explode("#",$postback['data']);
              $id_things=$getpostdata[1];
              $loadDb=$this->Dbs->getdata(array('id_things'=>$id_things),'things');
              $check=$loadDb->num_rows();
              if($check>0){
                $getData=$loadDb->row();
                $messages=[];
                $loadImg=$this->Dbs->getdata(array('id_things'=>$id_things),'image');
                if($loadImg->num_rows()>0){//check jika image ada didatabase
                  foreach ($loadImg->result() as $g) {
                    $image=base_url().'xfile/laporan/'.$g->name;
                    $msg2=$send->image($image);
                    array_push($messages,$msg2);
                  }
                  $msg1=$send->text($getData->desc);
                  array_push($messages,$msg1);
                }else{
                  $msg1=$send->text($getData->desc);
                  array_push($messages,$msg1);
                }
                $output=$send->reply($replyToken,$messages);

              }else{
                $message=array($send->text("Ups! barang sudah tidak ada ditemuan"));
                $output=$send->reply($replyToken,$message);
              }
            }
            if (substr($postback['data'],0,10)=='cek_barang'){//cek barang dari lokasi
              require_once 'class/Location.php';
              $location=new Location();
                $getpostdata=explode("#",$postback['data']);
                $status=$getpostdata[1];
                $id_official_location=$getpostdata[2];
                if($status=='hilang'){
                  $messages=$location->hilang($id_official_location,$userId);
                  $output=$send->reply($replyToken,$messages);
                }else if($status=='temuan'){
                  $messages=$location->temuan($id_official_location,$userId);
                  $output=$send->reply($replyToken,$messages);
                }
            }
            if (substr($postback['data'],0,11)=='next_barang'){//cek barang dari lokasi
              require_once 'class/Location.php';
              $location=new Location();
                $getpostdata=explode("#",$postback['data']);
                $status=$getpostdata[1];
                $id_official_location=$getpostdata[2];
                $nextPage=$getpostdata[3];
                if($status=='hilang'){
                  $messages=$location->hilang($id_official_location,$userId,$nextPage);
                  $output=$send->reply($replyToken,$messages);
                }else if($status=='temuan'){
                  $messages=$location->temuan($id_official_location,$userId,$nextPage);
                  $output=$send->reply($replyToken,$messages);
                }
            }
            if (substr($postback['data'],0,9)=='next_near'){//cek barang dari lokasi
                require_once 'class/Barang.php';
                $barang=new Barang();
                $getpostdata=explode("#",$postback['data']);//next_near#hilang#$radius#$lat#$lng#".($page+1)
                $status=$getpostdata[1];
                $radius=$getpostdata[2];
                $lat=$getpostdata[3];
                $lng=$getpostdata[4];
                $nextPage=$getpostdata[5];
                if($status=='hilang'){
                  $messages=$barang->hilang($radius,$lat,$lng,$userId,$nextPage);
                  if($messages===FALSE){
                    $messages=array($send->text("Ups! temuan tidak menemukan barang hilang dalam radius $radius km dari kamu"));
                  }
                  $output=$send->reply($replyToken,$messages);
                }else if($status=='temuan'){
                  $messages=$barang->temuan($radius,$lat,$lng,$userId,$nextPage);
                  if($messages===FALSE){
                    $messages=array($send->text("Ups! sepertinya belum ada barang temuan dalam radius $radius km dari kamu"));
                  }
                  $output=$send->reply($replyToken,$messages);
                }
            }
            if (substr($postback['data'],0,10)=='add_radius'){//cek barang dari lokasi
                require_once 'class/Barang.php';
                $barang=new Barang();
                $getpostdata=explode("#",$postback['data']);//add_radius#hilang#$radius#$lat#$lng
                $status=$getpostdata[1];
                $radius=$getpostdata[2];
                $lat=$getpostdata[3];
                $lng=$getpostdata[4];
                if($status=='hilang'){
                  $messages=$barang->hilang($radius,$lat,$lng,$userId);
                  if($messages===FALSE){
                    $messages=array($send->text("data barang hilang tetap tidak ditemukan dalam $radius km dari kamu"));
                  }
                  $output=$send->reply($replyToken,$messages);
                }else if($status=='temuan'){
                  $messages=$barang->temuan($radius,$lat,$lng,$userId);
                  if($messages===FALSE){
                    $messages=array($send->text("data barang temuan tetap tidak ditemukan dalam $radius km dari kamu"));
                  }
                  $output=$send->reply($replyToken,$messages);
                }
            }
            //POSTBACK END
            if (substr($postback['data'],0,5)=='claim'){
              $getpostdata=explode("#",$postback['data']);
              $status=$getpostdata[1];
              $id_things=$getpostdata[2];
              $getThings=$this->Dbs->getdata(array('id_things'=>$id_things),'things')->row();
              if($status=='lost'){
                //Cek nomor telfon
                if($db->phone==null){
                  $data['flag']='phoneUpdate';
                  $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                  if($sql){
                    $message=array($send->text("Ups! kamu belum mengatur nomor yang bisa dihubungi, silahkan balas bot ini dengan nomor telfon kamu"));
                    $output=$send->reply($replyToken,$message);
                    $client->replyMessage($output);
                  }
                }else{
                  $messages=array($send->text("Kamu baru saja memberitahu tentang info barang miliknya, temuan akan menghubungkan kamu segera"));
                  $messagesPush=[];
                  $msgP1=$send->text("Hei saya menemukan $getThings->name milik kamu");
                  //button
                  $buttons=[];
                  $button1 = array('type'=>'uri','label'=>'Telfon','uri'=>'tel:'.$db->phone);
                  $button2 = array('type'=>'postback','label'=>'Chat','data'=>'chat#'.$userId."#$id_things",'text'=>'Halo!');

                  array_push($buttons,$button1,$button2);
                  $msgP2=$send->buttonMessage(base_url()."xfile/images/teman.png","Temuan Cuy",substr($nama,0,25),"Gender : ".$db->gender,$buttons);
                  //button msg end
                  array_push($messagesPush,$msgP1,$msgP2);
                  $push  =$send->push($getThings->id_user,$messagesPush);
                  $client->pushMessage($push);
                }
                //Cek nomor telfon
              }else if($status=='found'){
                //Cek nomor telfon
                if($db->phone==null){
                  $data['flag']='phoneUpdate';
                  $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                  if($sql){
                    $message=array($send->text("Ups! kamu belum mengatur nomor yang bisa dihubungi, silahkan balas bot ini dengan nomor telfon kamu"));
                    $output=$send->reply($replyToken,$message);
                    $client->replyMessage($output);
                  }
                }else{
                  $messages=array($send->text("Kamu baru saja memberitahu tentang info barang yang kamu cari, temuan akan menghubungkan kamu segera"));
                  $messagesPush=[];
                  $msgP1=$send->text("Halo kak! itu  $getThings->name kayanya punya saya deh");
                  //button
                  $buttons=[];
                  $button1 = array('type'=>'uri','label'=>'Telfon','uri'=>'tel:'.$db->phone);
                  $button2 = array('type'=>'postback','label'=>'Chat','data'=>'chat#'.$userId."#$id_things",'text'=>'Halo!');

                  array_push($buttons,$button1,$button2);
                  $msgP2=$send->buttonMessage(base_url()."xfile/images/teman.png","Temuan Cuy",substr($nama,0,25),"Gender : ".$db->gender,$buttons);
                  //button msg end
                  array_push($messagesPush,$msgP1,$msgP2);
                  $push  =$send->push($getThings->id_user,$messagesPush);
                  $client->pushMessage($push);
                }
                //Cek nomor telfon
              }

              $output=$send->reply($replyToken,$messages);

            }

            if(substr($postback['data'],0,4)=='chat'){
              $getpostdata=explode("#",$postback['data']);
              $chat_id=$getpostdata[1];
              $id_things=$getpostdata[2];
              $checkstatus=$this->Dbs->getdata(array('id_things'=>$id_things),'things')->row();//cek dulu ke table things
              if($checkstatus->id_user2!=null){//kalo barang sudah ada user idnya maka akan
                $message=array($send->text("Gagal Terhubung, barang sudah ditemukan"));
                $output=$send->reply($replyToken,$message);
              }else{
                $check=$this->Dbs->getdata(array('id_user'=>$chat_id),'user')->row();//mengambil nilai user dari table barang hilang
                if($check->chat==null){//jika lawan chat tidak sedang dalam chat
                  $data=array(
                    'flag'=>'chat#'.$checkstatus->id_user.'#'.$id_things,
                    'chat'=>$chat_id
                  );
                  $sql=$this->Dbs->update(array('id_user'=>$userId),$data,'user');
                  if($sql){
                    $data2=array(
                      'flag'=>'chat#'.$checkstatus->id_user.'#'.$id_things,
                      'chat'=>$userId
                    );
                    $sql=$this->Dbs->update(array('id_user'=>$chat_id),$data2,'user');
                    if($sql){
                      $messages=[];
                      $msg1=$send->text("Berhasil terhubung dalam chat, katakan HI!");
                      $msg2=$send->text("untuk menghentikan obrolan ketik RESET atau KELUAR");
                      $msg3=$send->text("jika merasa sudah selesai dengan orang yang tepat silahkan ketik SELESAI atau BERES");
                      array_push($messages,$msg1,$msg2,$msg3);
                      $output=$send->reply($replyToken,$messages);
                    }
                  }
                }else{
                  $message=array($send->text("Teman ini belum bisa dihubungi dikarenakan sedang ada chat berlangsung"));
                  $output=$send->reply($replyToken,$message);
                }
              }

            }
        }  //END ELSE DARI PENGECEKAN DB FLAG

        if ($upPesan == 'GET@MYID'){
              $pre=array($send->text($userId));
              $output=$send->reply($replyToken,$pre);
          }
        if ($message['type']=='image'){
          // $imgprocess=$this->getMessage($messageid);
          // if($imgprocess!="error"){
          //   $pre=array($send->text("Sukses : ".$imgprocess));
          //   $output=$send->reply($replyToken,$pre);
          // }else{
          //   $pre=array($send->text("Gagal Upload"));
          //   $output=$send->reply($replyToken,$pre);
          // }

        }
        if ($upPesan == 'CEK') {
           if(!$reg->check($userId)){//pengecekan register
             $output=$send->reply($replyToken,$reg->message());
             $client->replyMessage($output);
             die;//proses tidak diteruskan jika kondisi ini terpenuhi
          }

          $ballons = [];
          $ballon1 = $send->text("Hasil : ");
          array_push($ballons, $ballon1);
          $output = $send->reply($replyToken, $ballons);

        }

        if ($upPesan =="QREPLY"){
          $messages=[];
          $items=[];
          $action1=$send->quickAction(array('type'=>'postback','label'=>'Hi','data'=>'hi','text'=>'hi kamu'));
          array_push($items,$action1);
          $msg1=$send->quickReply("Halo",$items);
          array_push($messages,$msg1);
          $output = $send->reply($replyToken, $messages);
        }

        if ($upPesan == '@@A')//pemanggilan TEXT biasa
              {
              $ballons = [];
              $ballon1 = $send->text("Text Pertama");
              $ballon2 = $send->text("Text Kedua");
              $ballon3 = $send->image("https://via.placeholder.com/450x400");

              array_push($ballons, $ballon1, $ballon2,$ballon3);
              $output = $send->reply($replyToken, $ballons);
              }
          if ($upPesan == '@@B')//pemanggilan button Template
              {
              $buttons=[];
              $button1 = array('type'=>'postback','label'=>'Test1','data'=>'lol');
              $button2 = array('type'=>'postback','label'=>'Test2','data'=>'lol');
              array_push($buttons,$button1,$button2);
              $ballons=[];
              $ballon1=$send->text("ini template");
              $ballon2=$send->buttonMessage("https://via.placeholder.com/450x400","ini alt","ini title","ini caption",$buttons);
              array_push($ballons,$ballon1,$ballon2);
              $output = $send->reply($replyToken,$ballons);
              }
          if ($upPesan == '@@C')//pemanggilan Confirm Template
              {
              $buttons=[];
              $button1 = array('type'=>'postback','label'=>'Test1','data'=>'lol');
              $button2 = array('type'=>'postback','label'=>'Test2','data'=>'lol');
              array_push($buttons,$button1,$button2);
              $ballons=[];
              $ballon1=$send->text("ini template");
              $ballon2=$send->confirmMessage("Alt","caption",$buttons);
              array_push($ballons,$ballon1,$ballon2);
              $output = $send->reply($replyToken,$ballons);
              }
          if ($upPesan == '@@D')//pemanggilan imageMap
              {
              $ballons=array($send->imagemap("https://islamify.id/dashboard/imagemap/mapfy/","ini alt"));//CONTOH REPLY 1 BALLON CHAT
              $output = $send->reply($replyToken,$ballons);

              }
              // $logs=json_encode($output);
              // $logsData['json']=$logs;
              // $logsData['id_user']=$userId;
              // $this->db->insert('logs_json', $logsData);
              $client->replyMessage($output);

  }

  function cekdb(){
    $check=$this->Dbs->getdata(array('id_user'=>'U4d764405f14220a7951fc5fab795495a','id_official_location'=>1),'circle')->num_rows();

  }

  function getMessage($idpesan){

    $curl = curl_init();
    curl_setopt_array($curl, array(
      CURLOPT_URL => 'https://api.line.me/v2/bot/message/'.$idpesan.'/content',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_HTTPHEADER => array(
        "authorization: Bearer Zp9Sz4p8N5rnosRwDt+CeGDHWa9yD9QAgZGMmFMo/Ehnty8ZUv46FZMlqs42C3C4jsMGzKmcI3SriBE98Dd6aLswgHoH8aNJWUsmC8EGozvfnwDaNNzntIZQy3UMpX1jomdlWSI0jItk6sDLbj2TqgdB04t89/1O/w1cDnyilFU=",
        "cache-control: no-cache",
        "postman-token: ca88e385-873e-ed46-2825-3c73eb879c63"
      ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
      return "error";
    } else {
      $filename=uniqid().'.jpeg';
      file_put_contents('xfile/laporan/'.$filename, $response);
      return $filename;
    }

  }



}
