<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');
  }

  function index()
  {

  }

  function auth(){
    if(isset($_POST['id']) && isset($_POST['password'])){
      $id_user=$_POST['id'];
      $password=md5($_POST['password']);
      $condition=array('id_user'=>$id_user,'password'=>$password);
      $loadDb=$this->Dbs->getdata('user',$condition);
      $check=$loadDb->num_rows();
      if($check>0){
        $getData=$loadDb->row();
        $data=array(
          'status'=>'success',
          'message'=>'berhasil login',
          'login'=>1,
          'id_user'=>$getData->id_user,
          'name'=>$getData->name,
          'gender'=>$getData->gender,
          'city'=>$getData->city,
          'phone'=>$getData->phone
        );
      }else{
        $data=array(
          'status'=>'success',
          'message'=>'username dan password salah',
          'login'=>0,
          'results'=>null
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;

  }

}
