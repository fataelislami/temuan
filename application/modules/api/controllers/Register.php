<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');

  }

  function index()
  {

  }

  function proses(){
  if(isset($_POST['id'])){//params yang akan dicek
    //default fungsi dari : getdata($table,$where=null,$limit=9,$offset=0){
    $isDuplicate=$this->Dbs->isDuplicate($_POST['id']);
    // var_dump($isDuplicate);
    if(!$isDuplicate){
      $id_user=$_POST['id'];
      $password=$_POST['password'];
      $name=$_POST['name'];
      $gender=$_POST['gender'];
      $city=$_POST['city'];
      $phone=$_POST['phone'];
      $package=array(
        'id_user'=>$id_user,
        'password'=>md5($password),
        'name'=>$name,
        'gender'=>$gender,
        'city'=>$city,
        'phone'=>$phone
      );
      $sql=$this->Dbs->insert($package,'user');
      if($sql){
        $data=array(
          'status'=>'success',
          'duplicate'=>0,
          // 'results'=>$get //Uncomment ini untuk contoh
        );
      }
    }else{
        $data=array(
          'status'=>'fail',
          'duplicate'=>1,
          // 'results'=>$get //Uncomment ini untuk contoh
        );
    }


  }else{
    $data=array(
      'status'=>'failed',
      'message'=>'parameter is invalid'
    );
  }
  $json=json_encode($data);
  echo $json;
}

}
