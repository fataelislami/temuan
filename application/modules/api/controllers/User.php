<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
  }

  function index()
  {

  }

  function information(){
    header('Content-Type: application/json');
    if (isset($_GET['id'])) {
      $id = $_GET['id'];
      $datauser      = $this->Dbs->getwhere('id_user',$id,'user');
      $laporantemuan = $this->Dbs->getcountlaporan_byuser($id,'found')->row();
      $laporanhilang = $this->Dbs->getcountlaporan_byuser($id,'lost')->row();
      $num_row       = $datauser->num_rows();
      if ($num_row > 0) {
        $user = $datauser->row();
        $get = array(
          'id_user'=> $user->id_user,
          'name'=> $user->name,
          'gender'=> $user->gender,
          'city'=> $user->city,
          'latitude'=> $user->latitude,
          'longitude'=> $user->longitude,
          'bitrthdate'=> $user->bitrthdate,
          'phone'=> $user->phone,
          'flag'=> $user->flag,
          'chat'=> $user->chat,
          'counter'=> $user->counter,
          'joined_date'=> $user->joined_date,
          'laporan_temuan'=>$laporantemuan->jumlah,
          'laporan_hilang'=>$laporanhilang->jumlah
        );
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$num_row,
          'results'=>$get,
        );
      }else {
        $data=array(
          'status'=>'success',
          'message'=>'not found',
          'total_result'=>$num_row,
        );
      }
    }else {
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json = json_encode($data);
    echo "$json";
  }

}
