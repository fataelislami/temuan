<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
  }

  function index()
  {
    echo "API : <br>";
    echo base_url()."api/laporan/byuser?id={id} <br>";
    echo base_url()."api/laporan/bylocation?id={id} <br>";
    echo base_url()."api/laporan/byid?id={id} <br>";
    echo base_url()."api/user/information?id=<br>";
    echo base_url()."api/location/information?id={id_official_location}";
  }

  function official_location(){
    header('Content-Type: application/json');
    if(isset($_GET['lat']) and isset($_GET['lng']) and isset($_GET['radius'])){
      $kilo=$_GET['radius'];
      $lat=$_GET['lat'];
      $lng=$_GET['lng'];
      $loadDb=$this->Dbs->getnear($kilo,$lat,$lng);
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->result();
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'results'=>$get,
        );
      }else{
        $data=array(
          'status'=>'success',
          'message'=>'not found',
          'total_result'=>$check
        );
      }


    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function get_near_things(){
    header('Content-Type: application/json');
    if(isset($_GET['lat']) and isset($_GET['lng']) and isset($_GET['radius']) and isset($_GET['status']) and isset($_GET['id_user'])){
      if(isset($_GET['page'])){
        $page=$_GET['page'];
      }else{
        $page=1;
      }
      $limitDb=9;
      $offsetDb=0;
      if($page!=1 and $page!=0){
        $offsetDb=$limitDb*($page-1);
      }
      $kilo=$_GET['radius'];
      $lat=$_GET['lat'];
      $lng=$_GET['lng'];
      $id_user=$_GET['id_user'];
      $status=$_GET['status'];
      $loadDb=$this->Dbs->get_things($kilo,$lat,$lng,$status,$id_user,$limitDb,$offsetDb);
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->result();
        $results=[];
        $imgDefault=base_url().'xfile/images/belum_ada_foto.png';
        foreach ($get as $g) {
          $loadImage=$this->Dbs->getdata('image',array('id_things'=>$g->id_things));
          if($loadImage->num_rows()>0){
            $imgDefault=base_url().'xfile/laporan/'.$loadImage->row()->name;
          }
          $item=array(
            'id_things'=>$g->id_things,
            'name'=>$g->name,
            'desc'=>$g->desc,
            'datetime'=>$g->datetime,
            'latitude'=>$g->latitude,
            'longitude'=>$g->longitude,
            'verification'=>$g->verification,
            'image'=>$imgDefault,
            'status'=>$g->status,
            'id_admin'=>$g->id_admin,
            'id_user'=>$g->id_user,
            'id_official_location'=>$g->id_official_location,
            'id_user2'=>$g->id_user2,
            'visible'=>$g->visible,
            'insert_time'=>$g->insert_time
          );
          array_push($results,$item);
        }
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'results'=>$results,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }

    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function mylocation(){
    header('Content-Type: application/json');
    if(isset($_GET['id_user'])){
      $loadDb=$this->Dbs->mylocation($_GET['id_user']);
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->result();
        $newData=[];//siapkan array baru untuk followers
        foreach ($get as $g) {
          $followers=$this->Dbs->getfollowers($g->id_official_location);
          $dataDB = array(
            'id_official_location' => $g->id_official_location,
            'id_user'=>$g->id_user,
            'username'=>$g->username,
            'name'=>$g->name,
            'image'=>$g->image,
            'city'=>$g->city,
            'latitude'=>$g->latitude,
            'longitude'=>$g->longitude,
            'followers'=>$followers
           );
           array_push($newData,$dataDB);
        }
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'results'=>$newData,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

}
