<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Location extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
  }

  function index()
  {

  }

  function information(){
    header('Content-Type: application/json');
    if (isset($_GET['id'])) {
      $id = $_GET['id'];
      $datalocation      = $this->Dbs->getwhere('id_official_location',$id,'official_location');
      $laporantemuan = $this->Dbs->getcountlaporan_bylocation($id,'found')->row();
      $laporanhilang = $this->Dbs->getcountlaporan_bylocation($id,'lost')->row();
      $num_row       = $datalocation->num_rows();
      if ($num_row > 0) {
        $location = $datalocation->row();
        $get = array(
          'id_official_location'=> $location->id_official_location,
          'username'=> $location->username,
          'name'=> $location->name,
          'image'=> $location->image,
          'city'=> $location->city,
          'latitude'=> $location->latitude,
          'longitude'=> $location->longitude,
          'id_user'=> $location->id_user,
          'insert_time'=> $location->insert_time,
          'lapotan_hilang'=>$laporanhilang->jumlah,
          'laporan_temuan'=>$laporantemuan->jumlah,
        );
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$num_row,
          'results'=>$get,
        );
      }else {
        $data=array(
          'status'=>'success',
          'message'=>'not found',
          'total_result'=>$num_row,
        );
      }
    }else {
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json = json_encode($data);
    echo "$json";
  }

}
