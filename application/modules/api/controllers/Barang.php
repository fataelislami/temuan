<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');
  }

  function index()
  {

  }

  function terbaru(){
    header('Content-Type: application/json');
    $this->db->order_by('insert_time', 'DESC');
      $loadDb=$this->Dbs->getdata('things');//database yang akan di load
      $check=$loadDb->num_rows();
      if($check>0){
        $get=$loadDb->result();
        $results=[];
        $imgDefault=base_url().'xfile/images/belum_ada_foto.png';
        foreach ($get as $g) {
          $loadImage=$this->Dbs->getdata('image',array('id_things'=>$g->id_things));
          if($loadImage->num_rows()>0){
            $imgDefault=base_url().'xfile/laporan/'.$loadImage->row()->name;
          }
          $item=array(
            'id_things'=>$g->id_things,
            'name'=>$g->name,
            'desc'=>$g->desc,
            'datetime'=>$g->datetime,
            'latitude'=>$g->latitude,
            'longitude'=>$g->longitude,
            'verification'=>$g->verification,
            'image'=>$imgDefault,
            'status'=>$g->status,
            'id_admin'=>$g->id_admin,
            'id_user'=>$g->id_user,
            'id_official_location'=>$g->id_official_location,
            'id_user2'=>$g->id_user2,
            'visible'=>$g->visible,
            'insert_time'=>$g->insert_time
          );
          array_push($results,$item);
        }
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$check,
          'results'=>$results,
        );
      }else{
        $data=array(
          'status'=>'success',
          'total_result'=>$check,
          'message'=>'not found'
        );
      }

    $json=json_encode($data);
    echo $json;
  }

}
