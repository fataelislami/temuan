<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model(array('Dbs'));
    header('Content-Type: application/json');

  }

  function index()
  {

  }

  function create(){
    if(isset($_POST['id_user'])){//params yang akan dicek
      $name=$_POST['name'];
      $desc=$_POST['desc'];
      $datetime=$_POST['datetime'];
      $latitude=$_POST['lat'];
      $longitude=$_POST['lng'];
      $id_user=$_POST['id_user'];
      $foto=$this->upload_foto('gambar');
      $filename=$foto['file_name'];
      $status=$_POST['status'];
      $package=array(
        'name'=>$name,
        'desc'=>$desc,
        'datetime'=>$datetime,
        'latitude'=>$latitude,
        'longitude'=>$longitude,
        'id_user'=>$id_user,
        'status'=>$status,
        'verification'=>'success'
      );
      $isDuplicate=$this->Dbs->isDuplicate($_POST['id_user']);
      if(!$isDuplicate){//jika kunci tamu tidak ada didalam table user
        $data=array(
          'status'=>'failed',
          'message'=>'foreign key doesn`t exist',
          // 'results'=>$name,
        );
      }else{
        $sql=$this->Dbs->insert($package,'things');
        if($sql){
          $insert_id = $this->db->insert_id();
          if($filename!=''){
            $packageImage=array(
              'name'=>$filename,
              'id_things'=>$insert_id
            );
            $this->Dbs->insert($packageImage,'image');
          }
          $data=array(
            'status'=>'success',
            'message'=>'inserted to database',
            'file_name'=>$filename,
            // 'results'=>$name,
          );
        }else{
          $data=array(
            'status'=>'success',
            'message'=>'failed to insert',
            // 'results'=>$name,
          );
        }
      }
    }else{
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json=json_encode($data);
    echo $json;
  }

  function byuser(){
    if (isset($_GET['id'])) {
      $userid = $_GET['id'];
      $datathings = $this->Dbs->getthing_byuser($userid);
      $num_row    = $datathings->num_rows();
      if ($num_row > 0) {
        $get  = $datathings->result();
        $results=[];
        $imgDefault=base_url().'xfile/images/belum_ada_foto.png';
        foreach ($get as $g) {
          $loadImage=$this->Dbs->getdata('image',array('id_things'=>$g->id_things));
          if($loadImage->num_rows()>0){
            $imgDefault=base_url().'xfile/laporan/'.$loadImage->row()->name;
          }
          $username_official_location="";
          $official_location="";
          if($g->id_official_location!=null){
            $loadLocation=$this->Dbs->getdata('official_location',array('id_official_location'=>$g->id_official_location))->row();
            $username_official_location=$loadLocation->username;
            $official_location=$loadLocation->name;
          }          $item=array(
            'id_things'=>$g->id_things,
            'name'=>$g->name,
            'desc'=>$g->desc,
            'datetime'=>$g->datetime,
            'latitude'=>$g->latitude,
            'longitude'=>$g->longitude,
            'verification'=>$g->verification,
            'image'=>$imgDefault,
            'status'=>$g->status,
            'id_admin'=>$g->id_admin,
            'id_user'=>$g->id_user,
            'id_official_location'=>$g->id_official_location,
            'username_official_location'=>$username_official_location,
            'official_location'=>$official_location,
            'id_user2'=>$g->id_user2,
            'visible'=>$g->visible,
            'insert_time'=>$g->insert_time,
            'phone'=>$g->phone
          );
          array_push($results,$item);
        }
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$num_row,
          'results'=>$results,
        );
      }else {
        $data=array(
          'status'=>'success',
          'message'=>'not found',
          'total_result'=>$num_row,
        );
      }
    }else {
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json = json_encode($data);
    echo $json;
  }

  function bylocation(){
    if (isset($_GET['id'])) {
      $id = $_GET['id'];
      $datathings = $this->Dbs->getthings_bylocation($id);
      $num_row    = $datathings->num_rows();
      $results=[];
      $imgDefault=base_url().'xfile/images/belum_ada_foto.png';
      if ($num_row > 0) {
        $get  = $datathings->result();
        $results=[];
        $imgDefault=base_url().'xfile/images/belum_ada_foto.png';
        foreach ($get as $g) {
          $loadImage=$this->Dbs->getdata('image',array('id_things'=>$g->id_things));
          if($loadImage->num_rows()>0){
            $imgDefault=base_url().'xfile/laporan/'.$loadImage->row()->name;
          }
          $username_official_location="";
          $official_location="";
          if($g->id_official_location!=null){
            $loadLocation=$this->Dbs->getdata('official_location',array('id_official_location'=>$g->id_official_location))->row();
            $username_official_location=$loadLocation->username;
            $official_location=$loadLocation->name;
          }
          $item=array(
            'id_things'=>$g->id_things,
            'name'=>$g->name,
            'desc'=>$g->desc,
            'datetime'=>$g->datetime,
            'latitude'=>$g->latitude,
            'longitude'=>$g->longitude,
            'verification'=>$g->verification,
            'image'=>$imgDefault,
            'status'=>$g->status,
            'id_admin'=>$g->id_admin,
            'id_user'=>$g->id_user,
            'id_official_location'=>$g->id_official_location,
            'username_official_location'=>$username_official_location,
            'official_location'=>$official_location,
            'id_user2'=>$g->id_user2,
            'visible'=>$g->visible,
            'insert_time'=>$g->insert_time
          );

          array_push($results,$item);
        }
        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$num_row,
          'results'=>$results,
        );
      }else {
        $data=array(
          'status'=>'success',
          'message'=>'not found',
          'total_result'=>$num_row,
        );
      }
    }else {
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json = json_encode($data);
    echo $json;
  }

  function byid(){
    if (isset($_GET['id'])) {
      $id = $_GET['id'];
      $datathings = $this->Dbs->getthing_byid($id);
      $num_row    = $datathings->num_rows();
      if ($num_row > 0) {
        $g = $datathings->row();
        $results=[];
        $imgDefault=base_url().'xfile/images/belum_ada_foto.png';
          $loadImage=$this->Dbs->getdata('image',array('id_things'=>$g->id_things));
          if($loadImage->num_rows()>0){
            $imgDefault=base_url().'xfile/laporan/'.$loadImage->row()->name;
          }
          $phone="";
          $loadUser=$this->Dbs->getdata('user',array('id_user'=>$g->id_user));
          if($loadUser->num_rows()>0){
            $phone=$loadUser->row()->phone;
          }
          $username_official_location="";
          $official_location="";
          if($g->id_official_location!=null){
            $loadLocation=$this->Dbs->getdata('official_location',array('id_official_location'=>$g->id_official_location))->row();
            $username_official_location=$loadLocation->username;
            $official_location=$loadLocation->name;
          }
          $item=array(
            'id_things'=>$g->id_things,
            'name'=>$g->name,
            'desc'=>$g->desc,
            'datetime'=>$g->datetime,
            'latitude'=>$g->latitude,
            'longitude'=>$g->longitude,
            'verification'=>$g->verification,
            'image'=>$imgDefault,
            'status'=>$g->status,
            'id_admin'=>$g->id_admin,
            'id_user'=>$g->id_user,
            'id_official_location'=>$g->id_official_location,
            'username_official_location'=>$username_official_location,
            'official_location'=>$official_location,
            'id_user2'=>$g->id_user2,
            'visible'=>$g->visible,
            'insert_time'=>$g->insert_time,
            'phone'=>$phone
          );
          array_push($results,$item);

        $data=array(
          'status'=>'success',
          'message'=>'found',
          'total_result'=>$num_row,
          'results'=>$results,
        );
      }else {
        $data=array(
          'status'=>'success',
          'message'=>'not found',
          'total_result'=>$num_row,
        );
      }
    }else {
      $data=array(
        'status'=>'failed',
        'message'=>'parameter is invalid'
      );
    }
    $json = json_encode($data);
    echo $json;

  }

  public function upload_foto($formname){
  $config['upload_path']          = './xfile/laporan';
  $config['allowed_types']        = 'gif|jpg|png|jpeg';
  $config['encrypt_name'] = TRUE;
  //$config['max_size']             = 100;
  //$config['max_width']            = 1024;
  //$config['max_height']           = 768;
  $this->load->library('upload', $config);
  $this->upload->do_upload($formname);
  return $this->upload->data();

  //Cara pemakaian
  //hidupkan object terlebih dahulu
  //misal
  //$foto=$this->upload_foto();
  }

}
