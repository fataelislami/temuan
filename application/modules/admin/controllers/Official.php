<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Official extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Official_model');
        $this->load->library('form_validation');
    }

    public function index()
    {

      $dataofficial=$this->Official_model->get_all();//panggil ke modell
      $datafield=$this->Official_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => 'admin/official/official_location_list',
        'sidebar'=>'admin/sidebar',
        'css'=>'admin/crudassets/css',
        'script'=>'admin/crudassets/script',
        'dataofficial'=>$dataofficial,
        'datafield'=>$datafield,
        'module'=>'admin',
        'titlePage'=>'official'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => 'admin/official/official_location_form',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/official/create_action',
        'titlePage'=>'{nama halaman}'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Official_model->get_by_id($id);
      $data = array(
        'contain_view' => 'admin/official/official_location_edit',
        'sidebar'=>'admin/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'admin/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'admin/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'admin/official/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'{nama halaman}'
       );
      $this->template->load($data);
    }


    public function create_action()
    {

        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
          $foto=$this->upload_foto('image');
            $data = array(
		'username' => $this->input->post('username',TRUE),
    'password' => md5($this->input->post('password',TRUE)),
		'name' => $this->input->post('name',TRUE),
		'city' => $this->input->post('city',TRUE),
    'image'=>$foto['file_name'],
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE)
	    );

      // var_dump($data);


            $this->Official_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('admin/official'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id_official_location', TRUE));
        } else {
          $id_user=$this->input->post('id_user',TRUE);
          if($id_user==''){
            $id_user=null;
          }
          if($_FILES['image']['size']!=0){
            unlink('assets/official/'.$this->input->post('pre_image',TRUE));//menghapus file
            $foto=$this->upload_foto('image');
            if($_POST['password']!=''){
              $password=md5($_POST['password']);
              $data = array(
      'username' => $this->input->post('username',TRUE),
      'password' => $password,
      'name' => $this->input->post('name',TRUE),
      'image'=>$foto['file_name'],
      'city' => $this->input->post('city',TRUE),
      'latitude' => $this->input->post('latitude',TRUE),
      'longitude' => $this->input->post('longitude',TRUE),
      'id_user' => $id_user
        );
            }else{
              $data = array(
      'username' => $this->input->post('username',TRUE),
      'name' => $this->input->post('name',TRUE),
      'image'=>$foto['file_name'],
      'city' => $this->input->post('city',TRUE),
      'latitude' => $this->input->post('latitude',TRUE),
      'longitude' => $this->input->post('longitude',TRUE),
      'id_user' => $id_user

        );
            }

          }else{
            if($_POST['password']!=''){
              $password=md5($_POST['password']);
              $data = array(
      'username' => $this->input->post('username',TRUE),
      'password' => $password,
      'name' => $this->input->post('name',TRUE),
      'city' => $this->input->post('city',TRUE),
      'latitude' => $this->input->post('latitude',TRUE),
      'longitude' => $this->input->post('longitude',TRUE),
      'id_user' => $id_user

        );
            }else{
              $data = array(
      'username' => $this->input->post('username',TRUE),
      'name' => $this->input->post('name',TRUE),
      'city' => $this->input->post('city',TRUE),
      'latitude' => $this->input->post('latitude',TRUE),
      'longitude' => $this->input->post('longitude',TRUE),
      'id_user' => $id_user
        );
            }

          }


            $this->Official_model->update($this->input->post('id_official_location', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('admin/official'));
        }
    }

    public function delete($id)
    {
        $row = $this->Official_model->get_by_id($id);

        if ($row) {
            $this->Official_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('admin/official'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('admin/official'));
        }
    }

    public function upload_foto($formname){
    $config['upload_path']          = './assets/official';
    $config['allowed_types']        = 'jpg|png|jpeg';
    $config['encrypt_name'] = FALSE;
    //$config['max_size']             = 100;
    //$config['max_width']            = 1024;
    //$config['max_height']           = 768;
    $this->load->library('upload', $config);
    $this->upload->do_upload($formname);
    return $this->upload->data();

    //Cara pemakaian
    //hidupkan object terlebih dahulu
    //misal
    //$foto=$this->upload_foto();
    }

    public function _rules()
    {
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('city', 'city', 'trim|required');


	$this->form_validation->set_rules('id_official_location', 'id_official_location', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}
