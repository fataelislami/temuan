<nav class="sidebar-nav">
    <ul id="sidebarnav">
        <li class="nav-small-cap">Menu</li>
        <!-- //BATAS SATU MENU -->
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin"><i class="mdi mdi-gauge"></i><span class="hide-menu">Home</span></a>
        </li>
        <!-- //BATAS SATU MENU -->
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin/account"><i class="mdi mdi-account"></i><span class="hide-menu">Superads</span></a>
        </li>
        <li>
            <a class="waves-effect waves-dark" href="<?php echo base_url()?>admin/official"><i class="mdi mdi-map-marker-multiple"></i><span class="hide-menu">Lokasi Penting</span></a>
        </li>

        <li>
            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-rocket"></i><span class="hide-menu">Barang</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="<?php echo base_url()?>admin/things">Lokasi Resmi</a></li>
                <li><a href="<?php echo base_url()?>admin/things/near">Lokasi Terdekat</a></li>
            </ul>
        </li>



        <!--
        <li>
        CONTOH MENU DENGAN SUBMENU
            <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-pencil"></i><span class="hide-menu">Tulisan</span></a>
            <ul aria-expanded="false" class="collapse">
                <li><a href="<?php echo base_url()?>admin/tulisan">Tulisan Saya</a></li>
                <li><a href="<?php echo base_url()?>admin/tulisan/all">Tulisan Creator</a></li>
            </ul>
        </li>
         -->
<!-- REFERENSI IKON UNTUK MENU : https://cdn.materialdesignicons.com/1.1.34/ -->
    </ul>
</nav>
