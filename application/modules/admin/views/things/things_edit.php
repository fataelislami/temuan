<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Things</h4>
            <form class="form-material m-t-40" method="post" action="<?php echo base_url().$action ?>">
	  <div class="form-group">
                    <label>ID Barang</label>
                    <input type="text" name="id_things" class="form-control" placeholder="" value="<?php echo $dataedit->id_things?>" readonly>
            </div>
	  <div class="form-group">
            <label>Nama Barang</label>
            <input type="text" name="name" class="form-control" value="<?php echo $dataedit->name?>">
    </div>
	  <div class="form-group">
            <label>Deskripsi Barang</label>
            <input type="text" name="desc" class="form-control" value="<?php echo $dataedit->desc?>">
    </div>
	  <div class="form-group">
            <label>verification</label>
            <input type="text" name="verification" class="form-control" value="<?php echo $dataedit->verification?>" readonly>
    </div>
	  <div class="form-group">
            <label>status</label>
            <input type="text" name="status" class="form-control" value="<?php echo $dataedit->status?>" readonly>
    </div>
	  <div class="form-group">
            <label>Nama Admin</label>
            <input type="text" name="id_admin" class="form-control" value="<?php echo $dataedit->nameadmin?>" readonly>
    </div>
	  <div class="form-group">
            <label>Nama User</label>
            <input type="text" name="id_user" class="form-control" value="<?php echo $dataedit->nameuser?>" readonly>
    </div>
	  <div class="form-group">
            <label>id_official_location</label>
            <select class="form-control" name="official" value="">
              <option value=""></option>
              <?php
                foreach ($dataofficial as $a) { ?>
                  <option value="<?php echo "$a->id_official_location"; ?>" <?php if($a->id_official_location == $dataedit->id_official_location){echo "selected";} ?>>
                    <?php echo "$a->name"; ?>
                  </option>
                <?php }
               ?>
            </select>
    </div>

                <div class="form-group">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
