<?php if($this->session->flashdata('message')) {
  $flashMessage=$this->session->flashdata('message');
echo "<script>alert('$flashMessage')</script>";
 } ?>
<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
              <div class="row">
                  <div class="col-md-6">
                      <h4 class="card-title" id="titleTest">Data Things</h4>
                      <h6 class="card-subtitle">Export data to Copy, CSV, Excel, PDF & Print</h6>
                  </div>
                  <div class="col-md-6 text-right">
                      <?php echo anchor(site_url($module.'/things/create'), '+ Tambah Data', 'class="btn btn-primary"'); ?>
      	    </div>
              </div>


                <div class="table-responsive m-t-40">
                    <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>Deskripsi</th>
                                <th>Status</th>
                                <th>Verifikasi</th>
                                <th>Aksi</th>
                                <th>Broadcast</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php foreach ($datathings as $d): ?>
                            <tr>
                                <td><?= $d->id_things ?></td>
                                <td><?= $d->name ?></td>
                                <td><?= substr($d->desc,0,20) ?></td>
                                <td><?= $d->status ?></td>
                                <?php if($d->verification=='pending'){ ?>
                                  <td><button  data-toggle="modal" data-target="#responsive-modal" class="btn btn-info waves-effect waves-light m-r-10 modalVerification" value="<?php echo $d->id_things ?>">Verifikasi</button></td>
                                <?php }else{ ?>
                                  <td><?= $d->verification ?></td>
                                <?php } ?>
                                <td>
                                          <!-- modal -->
                                          <button type="button" class="btn btn-primary waves-effect waves-light m-r-10" data-toggle="modal" data-target="#viewModal<?php echo $d->id_things ?>" data-whatever="@mdo" >View</button>
                                          <div class="modal fade bs-example-modal-lg" id="viewModal<?php echo $d->id_things ?>" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
                                                                  <div class="modal-dialog modal-lg" role="document">
                                                                      <div class="modal-content">
                                                                          <div class="modal-header">
                                                                              <h4 class="modal-title" id="myLargeModalLabel">Detail Barang</h4>
                                                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                                          </div>
                                                                          <div class="modal-body">

                                                                            <!-- Modal Body -->
                                                                            <div class="row">
                                                                              <div class="col-12">
                                                                                <div class="card">
                                                                                    <div class="card-body">
                                                                                      <table width="100%">
                                                                                        <tr>
                                                                                          <td>Nomor</td><td><?php echo $d->id_things?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                          <td>Name</td><td><?php echo $d->name?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                          <td>Deskripsi</td><td><?= substr($d->desc,0,20)?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                          <td>Nama User</td><td><?php echo $d->nameuser?></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                          <center>
                                                                                          <img src="https://islamify.id/bot/temuan/xfile/laporan/<?php echo "$d->imagename"; ?>" style="width:80%;height:auto;" alt="">
                                                                                        </tr>
                                                                                      </table>
                                                                                    </div>
                                                                                </div>
                                                                              </div>
                                                                            </div>
                                                                            <!-- /.model body -->

                                                                          </div>
                                                                          <div class="modal-footer">
                                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                          </div>
                                                                      </div>
                                                                  </div>
                                                              </div>
                                                              <!-- /.modal -->
                                  <a href="<?php echo base_url().$module?>/things/edit/<?php echo $d->id_things ?>">
                                         <button class="btn btn-success waves-effect waves-light m-r-10">Edit</button>
                                  </a>
                                         <button  data-toggle="modal" data-target="#responsive-modal" class="btn btn-danger waves-effect waves-light m-r-10 modalDelete" value="<?php echo $d->id_things ?>">Delete</button>
                                </td>
                                <td>
                                  <button  data-toggle="modal" data-target="#responsive-modal" class="btn btn-danger waves-effect waves-light m-r-10 modalBroadcast" value="<?php echo $d->id_things."#".$d->status ?>">Broadcast</button>
                                </td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>



                <!--  end modal -->
            </div>
        </div>
    </div>
</div>
<!-- <?php echo base_url().$module?>/things/delete/<?php echo $d->id_things ?> -->
<!-- delete modal content -->
<div id="responsive-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title">Temuan Hi!</h4>
            </div>
            <div class="modal-body">
              <p id="modalMsg"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Tutup</button>
                <a id="modalHref" href="#">
                <button type="button" class="btn btn-danger waves-effect waves-light">Yup!</button>
                </a>
            </div>
        </div>
    </div>
</div>
<!-- /.modal -->
