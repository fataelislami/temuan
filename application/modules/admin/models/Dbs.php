<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dbs extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  function getdata($where,$from){
    $this->db->where($where);
    $db=$this->db->get($from);
    return $db;
  }

  function getwhere($where,$value,$table){
    $this->db->where($where, $value);
    $db=$this->db->get($table);
    return $db;
  }

  function getnear($kilo,$lat,$lng){
    $this->db->select("*,`official_location`.`id_official_location`, ( 6371 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance,count(`circle`.`id_user`) as `followers`");
      $this->db->having('distance <= ' . $kilo);
      $this->db->order_by('distance');
      $this->db->limit(20, 0);
      $this->db->from('official_location');
      $this->db->join('circle', 'official_location.id_official_location = circle.id_official_location', 'left');
      $this->db->group_by("circle.id_official_location");
      $db = $this->db->get();
      return $db;
    }

    function get_things($kilo,$lat,$lng,$status,$id_user=null,$limit=9,$offset=0){
        $this->db->select("*, ( 6371 * acos( cos( radians($lat) ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians($lng) ) + sin( radians($lat) ) * sin( radians( latitude ) ) ) ) AS distance");
          $this->db->having('distance <= ' . $kilo);
          $this->db->order_by('distance');
          $this->db->limit($limit, $offset);
          $this->db->where('id_user!=', $id_user);
          $this->db->where('status', $status);
          $this->db->where('verification', 'success');
          $this->db->where('id_user2', null);
          $this->db->where('id_official_location', null);
          $db=$this->db->get('things');
          return $db;
    }

  function getfollowers($id_official_location){
    $sql="SELECT count(*) as `followers` from `circle` where `id_official_location`=$id_official_location";
    return $this->db->query($sql)->row()->followers;
  }
  function locationuser($userId){
    $sql="SELECT * FROM `user` JOIN `circle` USING (`id_user`) where `id_user`='$userId'";
    return $this->db->query($sql);
  }

  function mylocation($userId){
    $sql="SELECT * FROM `circle` JOIN `official_location` USING (`id_official_location`) where `circle`.`id_user`='$userId'";
    return $this->db->query($sql);
  }

  function insert($data,$table){
   $insert = $this->db->insert($table, $data);
   if ($this->db->affected_rows()>0) {
     return true;
     }else{
     return false;
     }
 }

 function update($data,$table,$where,$value){
    $this->db->where($where,$value);
    $db=$this->db->update($table,$data);
    if ($this->db->affected_rows()>0) {
      return true;
      }else{
      return false;
      }
  }

}
