<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('class/Image.php');

class Lapor extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->library( array('image_lib') );

  }

  function _remap($parameter){

   $this->index($parameter);
}


  function index($parameter)
  {
      $size=$this->uri->segment(3);
      $image=new Image();
    echo $image->resize(base_url()."/imagemap/lapor.png",800 ,800,$size,$size);

  }

}
