<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require_once('class/Image.php');

class Sebarin_revisi extends MY_Controller{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->library( array('image_lib') );

  }

  function _remap($parameter){

   $this->index($parameter);
}


  function index($parameter)
  {
      $size=$this->uri->segment(3);
      $image=new Image();
    echo $image->resize(base_url()."/imagemap/sebarin.png",1040 ,1040,$size,$size);

  }

}
