<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Official extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Official_model');
        $this->load->library('form_validation');
    }

    public function index()
    {

      $dataofficial=$this->Official_model->get_all();//panggil ke modell
      $datafield=$this->Official_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => '{namamodule}/official/official_location_list',
        'sidebar'=>'{namamodule}/sidebar',
        'css'=>'{namamodule}/crudassets/css',
        'script'=>'{namamodule}/crudassets/script',
        'dataofficial'=>$dataofficial,
        'datafield'=>$datafield,
        'module'=>'{namamodule}',
        'titlePage'=>'official'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => '{namamodule}/official/official_location_form',
        'sidebar'=>'{namamodule}/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'{namamodule}/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'{namamodule}/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'{namamodule}/official/create_action',
        'titlePage'=>'{nama halaman}'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Official_model->get_by_id($id);
      $data = array(
        'contain_view' => '{namamodule}/official/official_location_edit',
        'sidebar'=>'{namamodule}/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'{namamodule}/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'{namamodule}/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'{namamodule}/official/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'{nama halaman}'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'username' => $this->input->post('username',TRUE),
		'name' => $this->input->post('name',TRUE),
		'city' => $this->input->post('city',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
	    );

            $this->Official_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('{namamodule}/official'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id_official_location', TRUE));
        } else {
            $data = array(
		'username' => $this->input->post('username',TRUE),
		'name' => $this->input->post('name',TRUE),
		'city' => $this->input->post('city',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
	    );

            $this->Official_model->update($this->input->post('id_official_location', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('{namamodule}/official'));
        }
    }

    public function delete($id)
    {
        $row = $this->Official_model->get_by_id($id);

        if ($row) {
            $this->Official_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('{namamodule}/official'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('{namamodule}/official'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('username', 'username', 'trim|required');
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('city', 'city', 'trim|required');
	$this->form_validation->set_rules('latitude', 'latitude', 'trim|required|numeric');
	$this->form_validation->set_rules('longitude', 'longitude', 'trim|required|numeric');

	$this->form_validation->set_rules('id_official_location', 'id_official_location', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}