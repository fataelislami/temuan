<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Things extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Things_model');
        $this->load->library('form_validation');
    }

    public function index()
    {

      $datathings=$this->Things_model->get_all();//panggil ke modell
      $datafield=$this->Things_model->get_field();//panggil ke modell

      $data = array(
        'contain_view' => '{namamodule}/things/things_list',
        'sidebar'=>'{namamodule}/sidebar',
        'css'=>'{namamodule}/crudassets/css',
        'script'=>'{namamodule}/crudassets/script',
        'datathings'=>$datathings,
        'datafield'=>$datafield,
        'module'=>'{namamodule}',
        'titlePage'=>'things'
       );
      $this->template->load($data);
    }


    public function create(){
      $data = array(
        'contain_view' => '{namamodule}/things/things_form',
        'sidebar'=>'{namamodule}/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'{namamodule}/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'{namamodule}/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'{namamodule}/things/create_action',
        'titlePage'=>'{nama halaman}'
       );
      $this->template->load($data);
    }

    public function edit($id){
      $dataedit=$this->Things_model->get_by_id($id);
      $data = array(
        'contain_view' => '{namamodule}/things/things_edit',
        'sidebar'=>'{namamodule}/sidebar',//Ini buat menu yang ditampilkan di module admin {DIKIRIM KE TEMPLATE}
        'css'=>'{namamodule}/crudassets/css',//Ini buat kirim css dari page nya  {DIKIRIM KE TEMPLATE}
        'script'=>'{namamodule}/crudassets/script',//ini buat javascript apa aja yang di load di page {DIKIRIM KE TEMPLATE}
        'action'=>'{namamodule}/things/update_action',
        'dataedit'=>$dataedit,
        'titlePage'=>'{nama halaman}'
       );
      $this->template->load($data);
    }


    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'desc' => $this->input->post('desc',TRUE),
		'datetime' => $this->input->post('datetime',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
		'verification' => $this->input->post('verification',TRUE),
		'image' => $this->input->post('image',TRUE),
		'status' => $this->input->post('status',TRUE),
		'id_admin' => $this->input->post('id_admin',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
		'id_official_location' => $this->input->post('id_official_location',TRUE),
	    );

            $this->Things_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('{namamodule}/things'));
        }
    }



    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->edit($this->input->post('id_things', TRUE));
        } else {
            $data = array(
		'name' => $this->input->post('name',TRUE),
		'desc' => $this->input->post('desc',TRUE),
		'datetime' => $this->input->post('datetime',TRUE),
		'latitude' => $this->input->post('latitude',TRUE),
		'longitude' => $this->input->post('longitude',TRUE),
		'verification' => $this->input->post('verification',TRUE),
		'image' => $this->input->post('image',TRUE),
		'status' => $this->input->post('status',TRUE),
		'id_admin' => $this->input->post('id_admin',TRUE),
		'id_user' => $this->input->post('id_user',TRUE),
		'id_official_location' => $this->input->post('id_official_location',TRUE),
	    );

            $this->Things_model->update($this->input->post('id_things', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('{namamodule}/things'));
        }
    }

    public function delete($id)
    {
        $row = $this->Things_model->get_by_id($id);

        if ($row) {
            $this->Things_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('{namamodule}/things'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('{namamodule}/things'));
        }
    }

    public function _rules()
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('desc', 'desc', 'trim|required');
	$this->form_validation->set_rules('datetime', 'datetime', 'trim|required');
	$this->form_validation->set_rules('latitude', 'latitude', 'trim|required|numeric');
	$this->form_validation->set_rules('longitude', 'longitude', 'trim|required|numeric');
	$this->form_validation->set_rules('verification', 'verification', 'trim|required');
	$this->form_validation->set_rules('image', 'image', 'trim|required');
	$this->form_validation->set_rules('status', 'status', 'trim|required');
	$this->form_validation->set_rules('id_admin', 'id admin', 'trim|required');
	$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
	$this->form_validation->set_rules('id_official_location', 'id official location', 'trim|required');

	$this->form_validation->set_rules('id_things', 'id_things', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

}