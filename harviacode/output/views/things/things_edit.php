<div class="row">
  <div class="col-12">
    <div class="card">
        <div class="card-body">
            <h4 class="card-title">Tambah Things</h4>
            <form class="form-material m-t-40" method="post" action="<?php echo base_url().$action ?>">
	  <div class="form-group">
                    <label>id_things</label>
                    <input type="text" name="id_things" class="form-control" placeholder="" value="<?php echo $dataedit->id_things?>" readonly>
            </div>
	  <div class="form-group">
            <label>name</label>
            <input type="text" name="name" class="form-control" value="<?php echo $dataedit->name?>">
    </div>
	  <div class="form-group">
            <label>desc</label>
            <input type="text" name="desc" class="form-control" value="<?php echo $dataedit->desc?>">
    </div>
	  <div class="form-group">
            <label>datetime</label>
            <input type="text" name="datetime" class="form-control" value="<?php echo $dataedit->datetime?>">
    </div>
	  <div class="form-group">
            <label>latitude</label>
            <input type="text" name="latitude" class="form-control" value="<?php echo $dataedit->latitude?>">
    </div>
	  <div class="form-group">
            <label>longitude</label>
            <input type="text" name="longitude" class="form-control" value="<?php echo $dataedit->longitude?>">
    </div>
	  <div class="form-group">
            <label>verification</label>
            <input type="text" name="verification" class="form-control" value="<?php echo $dataedit->verification?>">
    </div>
	  <div class="form-group">
            <label>image</label>
            <input type="text" name="image" class="form-control" value="<?php echo $dataedit->image?>">
    </div>
	  <div class="form-group">
            <label>status</label>
            <input type="text" name="status" class="form-control" value="<?php echo $dataedit->status?>">
    </div>
	  <div class="form-group">
            <label>id_admin</label>
            <input type="text" name="id_admin" class="form-control" value="<?php echo $dataedit->id_admin?>">
    </div>
	  <div class="form-group">
            <label>id_user</label>
            <input type="text" name="id_user" class="form-control" value="<?php echo $dataedit->id_user?>">
    </div>
	  <div class="form-group">
            <label>id_official_location</label>
            <input type="text" name="id_official_location" class="form-control" value="<?php echo $dataedit->id_official_location?>">
    </div>
	
                <div class="form-group">
                  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Submit</button>
                </div>
            </form>
        </div>
    </div>
  </div>
</div>
